/**
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

/* eslint-disable global-require */

import {translate} from '@docusaurus/Translate';
import {sortBy} from '@site/src/utils/jsUtils';

/*
 * ADD YOUR SITE TO THE DOCUSAURUS SHOWCASE
 *
 * Please don't submit a PR yourself: use the Github Discussion instead:
 * https://github.com/facebook/docusaurus/discussions/7826
 *
 * Instructions for maintainers:
 * - Add the site in the json array below
 * - `title` is the project's name (no need for the "Docs" suffix)
 * - A short (≤120 characters) description of the project
 * - Use relevant tags to categorize the site (read the tag descriptions on the
 *   https://docusaurus.io/showcase page and some further clarifications below)
 * - Add a local image preview (decent screenshot of the Docusaurus site)
 * - The image MUST be added to the GitHub repository, and use `require("img")`
 * - The image has to have minimum width 640 and an aspect of no wider than 2:1
 * - If a website is open-source, add a source link. The link should open
 *   to a directory containing the `docusaurus.config.js` file
 * - Resize images: node admin/scripts/resizeImage.js
 * - Run optimizt manually (see resize image script comment)
 * - Open a PR and check for reported CI errors
 *
 * Example PR: https://github.com/facebook/docusaurus/pull/7620
 */

// LIST OF AVAILABLE TAGS
// Available tags to assign to a showcase site
// Please choose all tags that you think might apply.
// We'll remove inappropriate tags, but it's less likely that we add tags.
export type TagType =
  | 'favorite'
  | 'accompagner'
  | 'collaborer'
  | 'communautés'
  | 'former'
  | 'gabarit'
  | 'gestesprofessionnels'
  | 'hybrider';

 
// Add sites to this list
// prettier-ignore
const Users: User[] = [
  {
    title: 'Accompagner le passage de magistère à Magistère',
    description: 'Temps d\'accompagnement de la communauté Magistère pour le passage à la nouvelle version de Magistère et la migration de parcours',
    preview: require('/docs/Usages/img/acc_mag.jpg'),
    website: '/docs/Usages/accompagner_magistere',
    tags: ['favorite','hybrider','former','accompagner','collaborer'],
  },  
  
  {
    title: 'Utiliser le gabarit hybrider en un clic ou presque',
    description: 'Découvrir un parcours pour hybrider simplement et rapidement une formation',
    preview: require('/docs/Usages/img/entre_pairs_1.jpg'),
    website: '/docs/Usages/entre_pairs_1',
    tags: ['favorite','hybrider','former'],
  },
 
  {
    title: 'Découvrir l\'offre hybrider ses formations',
    description: 'Identifier un dispositif de formation, ses leviers, points de vigilance et perspectives d\'évolution',
    preview: require('/docs/Usages/img/entre_pairs_2.jpg'),
    website: '/docs/Usages/entre_pairs_2',
    tags: ['favorite','hybrider','former','accompagner','collaborer'],
  },

  /*
  Pro Tip: add your site in alphabetical order.
  Appending your site here (at the end) is more likely to produce Git conflicts.
   */
];

export type User = {
  title: string;
  description: string;
  preview: string | null; // null = use our serverless screenshot service
  website: string;
  tags: TagType[];
};

export type Tag = {
  label: string;
  description: string;
  color: string;
};

export const Tags: {[type in TagType]: Tag} = {
  favorite: {
    label: translate({message: 'Nouveautés'}),
    description: translate({
      message:
        'Nouveaux partages d\'usages',
      id: 'showcase.tag.favorite.description',
    }),
    color: '#e9669e',
  },

  accompagner: {
    label: translate({message: 'accompagner'}),
    description: translate({
      message:
        'Accompagner les personnels',
      id: 'showcase.tag.accompagner.description',
    }),
    color: '#a44fb7',
  },

  collaborer: {
    label: translate({message: 'collaborer'}),
    description: translate({
      message:
        'Collaborer entre enseignants',
      id: 'showcase.tag.collaborer.description',
    }),
    color: '#fe6829',
  },


  communautés: {
    label: translate({message: 'communautés'}),
    description: translate({
      message:
        'Les communautés professionnelles',
      id: 'showcase.tag.communauté.description',
    }),
    color: '#8c2f00',
  },

  former: {
    label: translate({message: 'former'}),
    description: translate({
      message: 'Former les personnels avec Magistère',
      id: 'showcase.tag.former.description',
    }),
    color: '#dfd545',
  },

  gabarit: {
    label: translate({message: 'gabarit'}),
    description: translate({
      message:
        'Proposition de gabarits',
      id: 'showcase.tag.gabarit.description',
    }),
    color: '#127f82',
  },

  gestesprofessionnels: {
    label: translate({message: 'gestes professionnels'}),
    description: translate({
      message: 'Gestes professionnels avec Magistère',
      id: 'showcase.tag.gestesprofessionnels.description',
    }),
    color: '#4267b2',
  },

  hybrider: {
    label: translate({message: 'hybrider'}),
    description: translate({
      message: 'Hybrider avec Magistère',
      id: 'showcase.tag.hybrider.description',
    }),
    color: '#39ca30',
  },




};

export const TagList = Object.keys(Tags) as TagType[];
function sortUsers() {
  let result = Users;
  // Sort by site name
  result = sortBy(result, (user) => user.title.toLowerCase());
  // Sort by favorite tag, favorites first
  result = sortBy(result, (user) => !user.tags.includes('favorite'));
  return result;
}

export const sortedUsers = sortUsers();
