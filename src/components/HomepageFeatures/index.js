import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';
import PresentationSVG from '@site/static/img/team_up.svg';
import DocumentationSVG from '@site/static/img/completed_tasks.svg';
import AccesSVG from '@site/static/img/adventure_map.svg';
import ConceptionsSVG from '@site/static/img/building_websites.svg';
import EntrepairsSVG from '@site/static/img/entre_pairs.svg';

export default function HomepageFeatures() {
  return (
<section className='features_src-components-HomepageFeatures-styles-module homepage'>
      <div className="container">
        <div className="row">
          <div className={clsx('col col--6')}>
            <div className={clsx('container padding-horiz--md')}>
              <div className={clsx('row tuile')}>
                <div className={clsx('col col--9')}>
                  <h3 class="homepage">Présentation</h3>
                  <p className={clsx('text--justify')}>Magistere est une plateforme dédiée à la formation des enseignants et personnels de l'Éducation nationale. Elle propose des parcours hybrides accompagnés, en autonomie et des communautés professionnelles pour développer vos compétences.
                  <br/>Principes de fonctionnement (lien à venir)<br/>
                  <br/>L'écosystème de formation (lien à venir)<br/> </p>
                </div>
                <div className={clsx('col col--3')}>
                  <PresentationSVG className="homeSVG"/>
                </div>          
              </div>
              <div className={clsx('row tuile')}>
                <div className={clsx('col col--3')}>
                  <DocumentationSVG className="homeSVG"/>
                </div> 
                <div className={clsx('col col--9')}>
                  <h3 class="homepage">La documentation</h3>
                  <p className={clsx('text--justify')}>Cette documentation vous guide pas à pas dans l’utilisation de la plateforme pour concevoir, animer des formations et vous former. Elle est rédigée par la communauté des acteurs de la formation hybride et à distance</p>
                </div>         
              </div>
          </div>
        </div>
          <div className={clsx('col col--6')}>
            <div class='tuile'>
              <h4 class="homepage">Les différents modes de connexion disponibles sont : </h4>
              <p className={clsx('cadre')}><img src="https://test-concepteurs.apps.education.fr/theme/image.php/rdc/theme/1731639620/login-school" class="p-1"/>
                <span class="p-1">Enseignement scolaire :</span>  Pour tous les personnels du Ministère de l'Éducation nationale (Académie, Ministère) et les personnels de Canopé, du Cned, de l'ONISEP. Il s'agit du "Hub de fédération du Ministère".
              </p>
              <p className={clsx('cadre')}><img src="https://test-concepteurs.apps.education.fr/theme/image.php/rdc/theme/1731639620/login-high-school" class="p-1"/>
                <span class="p-1">Enseignement supérieur :</span> Pour les étudiants et tous les personnels du Ministère de l'Enseignement supérieur et de la Recherche. Il s'agit du "Hub de fédération Education-Recherche".
              </p>
              <p className={clsx('cadre')}><img src="https://test-concepteurs.apps.education.fr/theme/image.php/rdc/theme/1731639620/login-hub" class="p-1"/>
                <span class="p-1">Hub partenaires :</span> Pour les personnels de l'enseignement agricole, des établissements français à l'étranger, des partenaires institutionnels.
              </p>
              <p className={clsx('cadre')}><img src="https://test-concepteurs.apps.education.fr/theme/image.php/rdc/theme/1731639620/login-other" class="p-1"/>
                <span class="p-1">Autre compte :</span>Pour les invités c'est à dire des comptes créés ponctuellement pour des intervenant extérieurs, ou des personnels hors Enseignement (Personnels territoriaux, Autres partenaires)
              </p>
              <p> Pour en savoir plus sur la connexion <a href="docs/Tutoriels/Aide%20connexion">suivez ce lien</a>.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}