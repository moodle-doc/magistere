# Suivre l'avancement, motiver, relancer 
## Objectifs de ce geste métier
* Suivre l'avancement des participants dans leur formation pour identifier les besoins d'accompagnement
* Relancer tous ou certains d’entre eux.
* Lutter contre le décrochage
* Encourager, motiver les participants.
## Que devez-vous faire ?
Dans un parcours hybride, les participants doivent organiser leur formation en fonction du calendrier défini par les formateurs, ce qui nécessite une planification de leur part. En tant que formateur, vous leur apporterez une aide méthodologique en leur rappelant, entre autres, les étapes clés du parcours, les activités à réaliser, les échéances... En particulier durant les phases asynchrones. 
Suivre l’avancement des participants, les solliciter et les relancer le cas échéant permet d’une part de limiter le sentiment de solitude et d’autre part de réduire l’abandon ou l’échec des participants. Encourager les participants et valoriser leur travail renforce leur motivation et leur engagement.
Pour cela les formateurs ont des moyens de communication internes à la plateforme (forum, messagerie interne, visio), mais aussi externe (messagerie professionnelle, téléphone, autres).
En ce qui concerne le suivi de l’avancée des participants dans la formation sur les ressources et activités Magistère, il existe un outil de suivi qui permet de visualiser l’avancement de tous les participants en temps réel.
Notez qu'il ne permet pas de connaître leur réussite ou non aux activités. 

## Comment devez-vous procéder ?
Il est important de s'assurer dès le départ que tous les participants se connectent à la plateforme. Tout d'abord pour accéder aux resosurves et faire les activités proposées. Mais aussi pour faciliter la communication entre les particpants et les formateurs.
Dans le cadre des formations hybrides avec des périodes de travail asynchrone, il est fréquent que certains particpants aient du mal à se dégager du temps pour faire le travail, ou rencontrent un problème technique, où bien encore pensent que les activités à distance sont facultatives.
Il est donc important d'instaurer un climat de confiance dans l'espace de travail qui favorisera l'engagement de tous.
### la liste des participants
Cette page liste les participants avec leur nom et prénom, les adresse mél et la date de leur dernière connexion. Durant la formation, les formateurs pourront vérifier les connexions et en cas de date ancienne prendre contact avec le participant en cliquant sur son avatar.
![TableauParticipantsParcours](https://minio.apps.education.fr/codimd-prod/uploads/upload_c1d24c6e640cc820bc6e2bac508d8ae2.png)
### Le bloc "Suivi de mes activités"
Ce bloc, lié au suivi d'achèvement, est un outil puissant pour le formateur qui peut suivre l'avancement de chaque participant dans sa formation. Cet outil donne la possibilité de : 
* Choisir parmi les activités et les ressources présentes dans le parcours, celles qui apparaîtront dans l’outil de suivi. 
* Voir celles réalisées ou non par les participants.
* D'envoyer un message à un ou plusieurs participants, si nécessaire.
![VueEnsembleDesParticipants](https://minio.apps.education.fr/codimd-prod/uploads/upload_afa9e42a4983d954c27696d093fa6172.png)
Pour en savoir plus sur ce bloc et l'achèvement d'activité, consultez l'article "Achèvement et suivi des participants"
