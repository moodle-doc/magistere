# Activité Sticky notes
## Objectif
 L'activité Sticky notes permet d'agréger des idées en mode collaboratif sur un mur de notes que l'on peut déplacer (post-it). Il est aussi possible de voter pour certaines notes et de créer des colonnes pour les organiser. 

![StickyNotesActiviteParticipant](../img/ActiviteStickyNotes/StickyNotesActiviteParticipant.jpg#printscreen#centrer)

L'intérêt d'une telle activité est de permettre à tous les participants de s'exprimer pour proposer des idées et de pouvoir les filtrer (par un vote) et/ou les organiser (avec les colonnes). 
## Paramètres de l'activité Sticky notes
Dans le sélecteur d'activités, choisir le nuage de mots et renseigner la page de paramètres.
* Le titre de l'activité 
* La description, à afficher ou pas sur la page du parcours 
* La définition d'un nombre maximum de note par personne 
* L'ajout du nom de l'auteur sur chaque note 
* Déplacement des notes
* Vision de toutes les notes
* Rotation des notes

![StickyNotes-GestionNotes](../img/ActiviteStickyNotes/StickyNotes-GestionNotes.png#printscreen#centrer)
* La gestion des votes (autoriser les votes ou pas, limiter le nombre de votes ou pas)

![StickyNotes-GestionVotes](../img/ActiviteStickyNotes/StickyNotes-GestionVotes.png#printscreen#centrer)

* La gestion des couleurs des notes

![StyckyNotes-GestionCouleurs](../img/ActiviteStickyNotes/StickyNotes-GestionCouleurs.png#printscreen#centrer)

## Exemple d'activité
### Principe et configuration
On souhaite faire une synthèse des perceptions des participants sur un sujet. On propose 3 couleurs de notes (jaune pour les critiques, vert pour les propositions et bleu pour les réussites). On limite à 3 le nombre de notes que peut proposer un participant. La configuration se fait comme présenté dans l'image ci-dessus.
### Fonctionnement pour un participant

Chaque participant peut contribuer en ajoutant des notes (en cliquant sur + sous le titre de la colonne) Une fois que le participant a déposé 3 notes, il ne peut plus en ajouter (les + se transforment en X)
### Le point de vue du formateur

Une fois que le temps imparti aux participants pour contribuer est écoulé, le formateur peut reprendre la main sur le tableau et structurer les notes, par exemple en 3 colonnes pour mieux les identifier. Si ce n'est pas déjà fait, il peut alors activer la possibilité de vote pour que les participants puissent choisir les propositions les plus importantes à conserver ou à valoriser. Cette configuration se fait dans la page de paramètres de l'activité comme ci-dessous : 

![StickyNotes-GestionVotes](../img/ActiviteStickyNotes/StickyNotes-GestionVotes.png#printscreen#centrer)

### Décodage de la vision 'participant'
![StickyNotes-ActiviteParticipant](../img/ActiviteStickyNotes/StickyNotesActiviteParticipant.jpg#printscreen#centrer)
* Le participant a proposé une note ("trop technique"), sur laquelle on peut voir les outils d'édition, suppression et déplacement.
* Il a voté pour deux notes ("pas naturel" et "nouvelles compétences avec les CV") car l'icône en forme de cœur est noire sur ces notes.
* Il voit le nombre de votes attribués à chaque note.
* Il peut encore déposer au moins une note (identifiable par les + dans les zones grises en haut des colonnes).
### Actions possibles pour le formateur
Sur le tableau, le formateur peut à tout moment :
* Ajouter / supprimer des colonnes (la suppression d'une colonne entraîne la suppression de toutes les notes qui sont dans cette colonne)
* Ajouter / modifier / déplacer / supprimer des notes
* Voter pour des notes, dans la limite du nombre de votes fixé dans l'activité

### Actions possibles pour le participant
* Ajouter des notes, dans la limite du nombre fixé dans l'activité
* Supprimer / modifier / déplacer ses propres notes sur le tableau
* Voter pour des notes, dans la limite du nombre de votes fixé dans l'activité
