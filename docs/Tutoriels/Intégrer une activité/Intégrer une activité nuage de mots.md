# Intégrer une activité Nuage de mots

## Objectif

Il est parfois pertinent de demander aux participants de s'exprimer sur une notion ou un concept avec un nombre limité de mots. L'activité **Nuage de mots** permet de réaliser dynamiquement une synthèse visuelle de l'ensemble des contributions.

![Exemple-de-nuage-de-mots](../img/nuagedemots/Exemple_de_nuage_de_mots.jpg#printscreen#centrer)

L'intérêt pédagogique d'une telle activité est de faire ressortir des tendances et cartographier le vocabulaire autour d'une notion. Il n'est pas possible de travailler plus spécifiquement l'esthétique du nuage (orientation des mots, couleurs, forme du nuage).

## Paramètres généraux de l'activité Nuage de mots

Dans le sélecteur d'activités, choisir le Nuage de mots et renseigner la page de paramètres :
* Le titre de l'activité
* La description, que vous pouvez afficher ou pas sur la page du parcours
* La consigne qui ne sera visible que sur la page de l'activité
* Le nombre maximum de mots que peut proposer un participant
* Le nombre minimum de mots que doit proposer un participant

![Nuage de Mots Paramètres Généraux](../img/nuagedemots/MuageMots-ParamGeneraux.png#printscreen#centrer)

## Paramètres d'achèvement de l'activité

Une seule condition d'achèvement automatique est proposée pour le nuage de mots : "Le participant doit proposer le nombre minimum de mots". Cette condition n'est utilisable que si l'on impose au participant de renseigner au moins un mot.
Il est possible de coupler cette condition avec une date limite.

### Exemple

L'image ci-dessous propose l'exemple d'un achèvement automatique si le participant a proposé au moins 2 mots (nombre minimum de propositions par participants fixé à 2 et condition d'achèvement activée) sans date limite.

![Nuage de Mots Achevement Activité](../img/nuagedemots/MuageMots-AchevActivite.png#printscreen#centrer)

## Fonctionnement pour un participant

Un participant voit, une fois rentré dans l'activité, l'interface comme ci-dessous : 

![Nuage de Mots - Saisie Partcipant](../img/nuagedemots/MuageMots-SaisieParticipant.png#printscreen#centrer)

On retrouve le titre de l'activité et la consigne, suivis d'un formulaire pour saisir les différentes proposition.

Les champs obligatoires sont au début du formulaire avec le point d'exclamation (dans notre exemple, les 2 premiers champs de réponse) suivi des champs facultatifs.
La description de l'activité n'est pas visible.

:::info
Le participant ne peut pas voir le nuage de mots tant qu'il n'a pas répondu pour ne pas être influencé.
Une fois que le participant a répondu, il voit le nuage, riche de l'ensemble des contributions, et ne peut plus contribuer.
:::

## Actions possibles pour le formateur

### Export PNG

Le formateur peut faire un export au format image (PNG) du nuage de mot pour avoir une trace de l'activité en cliquant sur le bouton **Exporter image**.

### Export des données

Il peut aussi réaliser un export des données au format compatible Excel en cliquant sur **Exporter les données**.

### Ajouter des mots

Le formateur peut ajouter des mots par la boîte de dialogue en bas de page et le bouton **Ajouter**.

### Modifier des mots

il est possible de modifier chaque mot (pour des raisons d'orthographe, par exemple) en cliquant sur le mot en question. 

![Nuage_de_mots_-_vue_formateur](../img/nuagedemots/Nuage_de_mots_-_vue_du_formateur.jpg#printscreen#centrer)

