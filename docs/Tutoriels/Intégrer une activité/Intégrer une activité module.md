# Intégrer une activité module

## Principe de fonctionnement

l’activité « module » permet d'intégrer d’autres espaces au vôtre. Pour pouvoir lier un espace il doit être :
* issus des parcours où l’on est formateur ;
* issus du catalogue en libre accès (autoformation, communautés...).

## Créer l'activité

Tout d'abord, vérifier que le mode édition du parcours est activé.

![Edition1bis](../img/ActivitéModule/Edition1bis.PNG#printscreen#centrer)

Cliquer sur la partie "Ajouter une activité ou ressource".

![Ajout_activité](../img/ActivitéModule/Ajout_activité.PNG#printscreen#centrer)

Puis choisir l'activité module parmi les activités proposées.

![Ecran_ajout](../img/ActivitéModule/Ecran_ajout.PNG#printscreen#centrer)


Ou, rechercher l'activité module dans le bloc "Menu édition" à droite.

![Menu_édition](../img/ActivitéModule/Menu-édition.PNG#printscreen#centrer)

Une fois sur la page module, on paramètre le module.

![Paramétrage_module](../img/ActivitéModule/Paramétrage_module.PNG#printscreen#centrer)

Lors du paramétrage du module, on choisit parmi la liste des parcours.

![Choix_parcours_bis](../img/ActivitéModule/Choix_parcours_bis.png#printscreen#centrer)

![Choix_parcours bis1](../img/ActivitéModule/Choix_parcours%20bis1.PNG#printscreen#centrer)

Une fois l'activité paramétrée, et enregistrée, l'activité apparaît comme ci-dessous dans la section choisie.

![Ecran_final_module](../img/ActivitéModule/Ecran_final_module.PNG#printscreen#centrer)










