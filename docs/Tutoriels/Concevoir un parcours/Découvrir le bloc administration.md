---
tags:
  - administration
  - participants
---



# Découvrir le bloc administration

Le bloc administration se présente sous la forme d'onglets en haut de l'espace. La plupart des fonctions sont accessibles directement mais pour avoir accès à l'ensemble, il est préférable d'être en mode édition. 

![Bloc administration capture barre](../img/blocadministration/blocadministrationbarre.png#printscreen#centrer)


## Onglet Cours
Cet onglet permet de revenir à la page d'accueil de votre espace.

## Onglet Paramètres
Dans cet onglet, il est possible de changer l'intitulé de votre espace, d'indiquer les informations quant à son articulation : durée, date de début et date de fin. 
Un encart description permet de le décrire plus en détail : objectifs, modalités. Il est possible d'y ajouter divers médias : images, vidéos, fichiers. 

Différents onglets permettent d'affiner l'organisation du parcours :

![Paramètres disponibles : Format de cours, Apparence, Fichier et dépôts, suivi d'achèvement Groupes, Renommer les rôles, Mots-clés](../img/blocadministration/ongletparamètres.png#printscreen#centrer)

## Onglet Participants
Cet onglet permet de gérer les inscriptions, les groupes et les rôles des participants de votre espace.

Après avoir cliqué sur cet onglet, un menu déroulant apparait en haut à gauche.

![capture d'écran du menu déroulant](../img/blocadministration/menuderoulantparticipants.png#printscreen#centrer)

Pour découvrir ces fonctionnnalités consulter la fiche **[Inscrire des participants à un espace](../Gérer%20les%20participants/Inscrire%20des%20participants%20à%20un%20parcours.md)**

## Onglet Notes
Cet onglet permet d'accéder aux notes des participants et affiche le rapport de l'évaluateur. En ouvrant le menu déroulant, on obtient les éléments suivants : 
- Rapport de l’évaluateur
- Historique d’évaluation
- Rapport d'ensemble
- Résumé des notes


## Onglet Rapports

Cet onglet permet d'avoir accès aux rapports globaux de l'espace, aux journaux et statistiques de l'espace.

![Onglet_rapports capture d'écran](../img/blocadministration/Onglet_rapports.PNG#printscreen#centrer)

## Onglet Plus

Cet onglet permet d'avoir accès à d'autres éléments de configuration :
* **Banque de questions** : créer et gérer les questions pour l'activité test
* **Banque de contenus** :   créer et gérer  des contenus H5P
* **Achèvement de cours** :   définir les critères d'achèvement de l'espace.
* **Badges** :   créer et gérer les badges.
* **Compétences** :  accès aux référentiels de compétences afin de définir les compétences visées lors d'une formation.
* **Filtres** : permet de désactiver/ activer les filtres (lien vers les activités, Generico,etc.).
* **[Réutilisation de cours](../Créer%20et%20gérer%20des%20parcours/Restaurer%20un%20parcours#printscreen#centrer)** : permet la sauvegarde de l'espace, la restauration d'un espace, etc.




