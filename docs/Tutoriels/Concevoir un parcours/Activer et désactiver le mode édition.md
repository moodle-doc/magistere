# Activer et désactiver le mode édition
Afin de concevoir des activités dans Magistère, le mode édition doit être actif. 

## Mode édition activé
 
Une fois que l'on est dans un parcours en tant que formatrice/formateur, le mode édition se situe en haut à droite près de votre menu profil. Il est automatiquement activé. On peut donc ajouter ou modifier des activités et des ressources. 
On remarque que le bouton est positionné à droite et qu'il est coloré. 

![Edition1bis](../img/Activerdesactivermodeedition/Edition1bis.PNG#printscreen#centrer)

## Mode édition désactivé
Si l'on souhaite désactiver le mode édition par exemple pour voir le rendu de la conception avec une vue formateur, il suffit de cliquer sur "mode édition". Le bouton se positionne à gauche et devient grisé.

![Edition2](../img/Activerdesactivermodeedition/Edition2.PNG#printscreen#centrer)

Pour activer à nouveau le mode édition, on clique sur "mode édition".


## Mode vue participant
Il est possible de voir le parcours en mode participant pour constater le rendu. Cela implique que le mode édition sera désactivé. 
 
* Pour obtenir la vue participant, on déroule le menu profil et on clique sur "prendre le rôle..."

![Participant1](../img/Activerdesactivermodeedition/Participant1.PNG#printscreen#centrer)


* Puis on sélectionne "participant"


![Participant2](../img/Activerdesactivermodeedition/Participant2.PNG#printscreen#centrer)

* Dans ce cas, le mode édition est désactivé automatiquement et n'apparaît plus.

![Participant3](../img/Activerdesactivermodeedition/Participant3.PNG#printscreen#centrer)


* En cliquant sur "retour à mon rôle normal", on retrouve le parcours en vue "formateur" mais avec le mode édition désactivé.

![Participant4](../img/Activerdesactivermodeedition/Participant4.PNG#printscreen#centrer)
