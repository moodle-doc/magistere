# Scénariser un parcours
**La scénarisation est une étape cruciale dans la conception d'une formation, garantissant sa qualité et son efficacité.**
Du côté du formateur, elle assure la cohérence entre les objectifs, les contenus et méthodes employés et les activités réalisées.
Du côté des participants, elle offre un cadre structuré et rassurant, clarifiant le fil conducteur, les objectifs à atteindre et les étapes de leur progression.
**Pour garantir cet alignement pédagogique ([Biggs, 1996](https://enseigner.hec.ca/pedagogie/alignement-pedagogique/)), nous présentons ci-dessous les différentes étapes de la scénarisation de la formation.**


## Analyse des besoins et des objectifs pédagogiques

***Cette étape est cruciale car elle permet d'identifier les besoins qui serviront à orienter le dispositif de formation. Quelques éléments sont à considérer :***
* contexte
* public cible
* contraintes de temps et de budget
* type de dispositif (présentiel, hybride, distanciel)
* ressources disponibles

## Conception du scénario pédagogique

***Cette étape est celle de la construction de l'architecture de la formation. Elle peut s'organiser en deux temps :***

**1. Élaboration du macro-design** : définition des objectifs, planification de la séquence de formation, choix des méthodes, des modalités, pour favoriser l'engagement et promouvoir la collaboration entre pairs.


:+1: A ce niveau, [un tableur](https://nuage07.apps.education.fr/index.php/s/ommsnDXLFYM4ZWz) peut aider à formaliser cette architecture !

----


**2. Définition du micro-design** : sélection de chaque outil et média propre à chaque temps de formation

:+1: [Un outil de conception](https://dane.ac-reims.fr/dokiel/moodle/draganddrop_ABC.html) basé sur la [méthode ABC Learning Design](https://abc-ld.org/sur/), proposé par la DRANE Grand-Est, peut faciliter les choix pédagogiques.
Le [Réseau des concepteurs](https://concepteurs.apps.education.fr/) proposera prochainement un outil de conception. 


## Réalisation du scénario pédagogique

***Cette étape consiste à produire les médias et les outils définis précédemment.
En perspective de la phase suivante, il peut être intéressant de connaître le potentiel des activités et ressources proposées sur Magistère.***

:+1: [Un guide des outils Moodle](https://moodletoolguide.net/fr/) pour enseignants et formateurs est proposé par la DRANE Grand-Est.

## Implémentation sur le LMS 

***Cette étape consiste à déployer la formation sur Magistère avec la création d'un parcours, afin de donner accès aux formateurs et aux participants à la formation.***

Des éléments importants doivent retenir l'attention des concepteurs :
* Simplifier l'architecture des sections
* Limiter le nombre de clic et le "scrolling"
* Guider les utilisateurs sur les actions à réaliser
* Respecter les principes d'accessibilité

## Évaluation de l'efficacité du scénario pédagogique

***Cette étape consiste à déterminer, dans différents domaines, à différents moments et à différents niveaux, si le dispositif a atteint ses objectifs, afin d'en analyser les impacts dans un objectif d'amélioration continue de la formation.*** 

Nous pouvons penser l'évaluation de différents domaines : pédagogique, tutoral, technologique, participation.

Cette évaluation s'intègre à différents moments de la formation comme le présente cette infographie. Voici un exemple de cadre d'évaluation.

![Cadre de l'évaluation Avant : besoins, test connaissances, auto-positionnement de compétences, validation des pré-requis. Pendant: vérification et consolidation des connaissances, validation des compétences, renforcement actif, maintien de l'attention. Après: évaluation à chaud des connaissances, satisfaction (J+1), validation des acquis, évaluation à froid des compétences et nouveaux comportements](../img/scenariserunparcours/Cadre_evaluation.png#printscreen#centrer)
 


Un exemple de questionnaire proposé par [Jonathan Pottiez](https://shs.cairn.info/publications-de-Jonathan-Pottiez--724940?lang=fr) (expert en évaluation de la formation), basé sur le modèle de [Kirkpatrick](https://evaluationformation.fr/modele-kirkpatrick/), permet d'évaluer les différents niveaux du domaine pédagogique, après la fin de la formation.

| Niveau 1 Réaction                                                      | Niveau 2  Apprentissage                        | Niveau 3  Comportement                                                   |Niveau 4           Résultats      |                                           
| ------------------------------------------------------------------------ | ----------------------------------------------- | ------------------------------------------------------------------------- | -------------------------------------------------------------------- |
| Quel est votre avis général sur la formation que vous venez de suivre  ? | Quels sont les messages clés que vous retenez ? | Quelles pratiques avez-vous commencé à changer/mettre en oeuvre depuis ? | Quels effets/impacts avez-vous pu observer suite à ces changements ? |

## L'essentiel à retenir
1. Les étapes sont successives mais aussi itératives pour corriger les défauts et optimiser la conception.
2. L'étape importante est la définition des objectifs pédagogiques ; vous pouvez vous aider de [la taxonomie de Bloom ici.](https://enseigner.hec.ca/pedagogie/creation-nouveau-cours/objectifs/)
