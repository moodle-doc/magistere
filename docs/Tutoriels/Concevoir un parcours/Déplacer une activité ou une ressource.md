

<!--tags:
  - conception
  - activité
  - ressource
 -->

# Déplacer une activité ou une ressource
## Contexte
Lors de la phase d’intégration dans Magistère, il arrive que des activités ou des ressources ne soient pas (ou plus) placées à l'endroit souhaité, par exemple suite à une évolution du scénario...
Il est alors nécessaire de les déplacer. Il existe 3 méthodes différentes de le faire.

## Par l'index
En plaçant la souris sur une activité ou une ressource une flèche en forme de croix apparaît.

![Capture Flèche Déplacement Ressource](../img/deplaceractivitéressource/FlechesDeplacementRessource.png#printscreen#centrer)

Double-cliquer et rester appuyé sur la ressource ou l’activité.
Déplacer l’élément à l’endroit souhaité à l’intérieur de l’index et relâcher.
:::info
Une section peut de la même manière être déplacée à un autre endroit de l’index avec tout son contenu (à l'exception de la section Généralités)
:::


## Par déplacement manuel du bloc de l’élément
Dans la zone d’affichage de votre contenu en passant la souris sur le bloc d’une ressource ou d’une activité, le curseur se transforme de flèche en croix.
Double-cliquer et rester appuyé puis glisser-déposer l’élément vers son nouvel emplacement. 
:::info
Cette méthode et n’est pas évidente et demande de la dextérité. Elle est intéressante si le déplacement est proche. Si le nouvel emplacement est éloigné, il vaut mieux privilégier l’index ou le menu de gestion.
:::

## Par le menu de gestion de l’élément
Dans le bloc de l’élément à déplacer, cliquer sur les trois points verticaux à droite et sélectionnez « Déplacer »

![Capture mune gestion ressources](../img/deplaceractivitéressource/MenuGestionRess-Act.png#printscreen#centrer)

Une boite de dialogue s'ouvre.
Celle-ci affiche l’ensemble de la structure de votre espace. Il reste à choisir **l’élément au-dessous duquel** viendra se placer la ressource ou l'activité à déplacer. A noter que les sections se deploient pour en afficher le contenu.

![Boite de Dialogue Déplacer](../img/deplaceractivitéressource/BoiteDialogueDeplacer.png#printscreen#centrer)

:::info
Cette méthode est la plus « sûre » car elle permet de choisir l’emplacement avec plus de précision
:::

