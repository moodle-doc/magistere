
# Inscrire des participants à un parcours

## À partir d'une adresse de messagerie

1. Sur la barre d'administration de l'espace, sélectionner l'onglet **participants**
2. Dans le sous menu déroulant, choisir ''**inscrire des utilisateurs par adresses mail**"

![inscrire1](../img/inscriredesparticipants/inscrire1.png#printscreen#centrer)

3. Copier les adresses mail dans l'encart prévu à cet effet. Chaque adresse doit être séparée par une virgule.

![inscrire2](../img/inscriredesparticipants/inscrire2.png#printscreen#centrer)

4. Choisir le **rôle** pour toute la liste des utilisateurs à inscrire

![inscrire3](../img/inscriredesparticipants/inscrire3.png#printscreen#centrer)

5. Cliquer sur **inscrire les utilisateurs** pour valider

![inscrire2bis](../img/inscriredesparticipants/inscrire2bis.png#printscreen#centrer)


##  À partir de la base des utilisateurs de Magistère

>  *important : cette méthode ne fonctionne que si les utilisateurs se sont connectés au moins une fois à Magistère.*

1. À partir de l'onglet **participants**, sélectionner **utilisateurs inscrits**

2. Cliquer sur le bouton **inscrire des utilisateurs**

![inscrire4](../img/inscriredesparticipants/inscrire4.png#printscreen#centrer)

3. Dans le menu **rechercher**, saisir le nom de famille de l'utilisateur, puis choisir dans la liste proposée l'utilisateur correspondant

4. Attribuer les rôles

![inscrire5](../img/inscriredesparticipants/inscrire5.png#printscreen#centrer)

5. Cliquer sur **inscrire des utilisateurs** pour valider
6. Il est possible d'ajouter plusieurs noms avant de cliquer sur **inscrire des utilisateurs**


