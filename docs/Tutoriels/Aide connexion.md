# Aide à la connexion à Magistère

Pour vous connecter à Magistère, il faut sélectionner le profil vous correspondant. Les différentes situations sont développées ci-dessous.

## Enseignement scolaire
![Enseignement scolaire](img/AideConnexion/enseignement_scolaire.png#printscreen#centrer)

Tous les personnels appartenant à l'Éducation nationale (Académie, Ministère) et les personnels de Canopé, du Cned, de l'ONISEP.
Il s'agit du "Hub de fédération du Ministère".

## Enseignement supérieur
![Enseignement supérieur et Recherche](img/AideConnexion/enseignement_superieur.png#printscreen#centrer)


Tous les personnels appartenant à l'Énseignement supérieur et Recherche.
Il s'agit du "Hub de fédération Education-Recherche".

## Partenaires 
![Partenaires](img/AideConnexion/Partenaires.png#printscreen#centrer)

Tous les personnels appartenant à l'AEFE et l'enseignement agricole (EducAgri)

## Autre compte
![Autre compte](img/AideConnexion/Autre_Compte.png#printscreen#centrer)

Les comptes locaux créé ponctuellement pour des intervenant extérieurs, ou des personnels hors Enseignement (Personnels territoriaux, Autres partenaires)

# Ce qui se passe après votre choix

## Connexion sur votre portail d'authentification

Selon votre situation vous allez être redirigés sur votre portail de connexion local ou la page Moodle de connexion pour la catégorie Autre compte.

## Redirection vers Magistère
Lorsque vous avez suivi les étapes de connexion, vous êtes redirigés vers la plateforme correspondant à votre profil.