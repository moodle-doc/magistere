# Restriction d'accès
## Introduction
Pour toutes les ressources et activités, il est possible d’en restreindre l’accès selon un ou plusieurs critères.
Les usages communs sont de proposer des ressources ou des activités à des groupes, à une date particulière (ou des les cacher après une date), etc.
Il est aussi possible de conditionner l’affichage d’une ressource ou d'une activité à une action effectuée sur une autre ressource ou activité.
## Comment faire
Dans les paramètres de la ressource ou de l’activité, aller à la partie "Restreindre l’accès" et cliquer sur le bouton "Ajouter une restriction".

![Parametreage de la Restriction d'Accès](../img/restrictionacces/ParametreRestrictionAcces.png#printscreen#centrer)

Vous pourrez alors choisir la ou les restrictions voulues. Si vous utilisez les groupes, vous aurez le choix "groupe" en plus.

![Critere de Restriction](../img/restrictionacces/CritereRestrictionAcces.png#printscreen#centrer)

Les critères les plus souvent retenus sont le groupe, la date et parfois le rôle. Vous pouvez utiliser la note à la condition que celle-ci ait été définie dans l’activité requise.
Une fois le critère choisi, il convient de le configurer. 
Par exemple, pour la restriction de groupe, vous pouvez décider si le participant doit ou non appartenir à un (ou plusieurs) groupe.

![Condition de Restriction](../img/restrictionacces/ConditionRestriction.png#printscreen#centrer)

:::info
si vous souhaitez que les participants non concernés par la ressource ou l’activité ne la voient pas, il faut penser à fermer l‘icône Œil. Sinon, l’élément n’est pas cliquable mais n’est pas caché.
:::

La restriction d'accès se matérialise de la manière suivante dans le parcours.

![Affichage de la Restriction dans le Bloc Activité](../img/restrictionacces/AffichageRestrictionBlocActivite.png#printscreen#centrer)

## Restriction et achèvement d'activité
Lier la restriction à l’achèvement d’activité permet un réglage fin de votre cours. Vous pouvez par exemple définir qu’une ressource n’apparaisse que si une activité (ou une ressource) antérieure a été marquée comme terminée. 
Les critères d’achèvement sont à choisir par vos soins. 
:::info
Dans ce cas, laisser apparaître l’accès restreint (en laissant l'oeil ouvert) peut être judicieux.
:::

## Restriction d'une section

Il est possible de restreindre l’accès d’une section comme pour les activités. La démarche est la suivante :
    • Passer en mode édition
    • Cliquer sur les trois points en regard du nom de la section et choisissez "Modifier la section".
    • Faire la restriction d’accès.
Les ressources et activités contenues dans la section héritent de de la restriction d'accès de la section.

## Utiliser des restrictions négative

Si vous utilisez plusieurs restrictions négatives (l'activité NE doit PAS être finalisée), il faut faire attention à la combinatoire logique ET / OU entre ces différentes restrictions.
