# Restaurer un parcours
## Étape 1 : sauvegarde du parcours
Dans un premier temps, le parcours à restaurer doit être sauvegardé.
Une fois que la sauvegarde a été faite, on obtient un fichier .mbz à télécharger sur l'ordinateur.
C'est ce fichier qui va être utilisé pour la sauvegarde.

## Étape 2 : restauration du parcours
* Une fois sur la page d'accueil de Magistère, "Créer un espace"

![Créer un espace](../img/restaurer/creer_espace.png#printscreen#centrer)

* Entrer le titre de l'espace

![Titre du parcours](../img/restaurer/Titre_du_parcours.PNG#printscreen#centrer)

* Sur la page centrale, cliquer sur l'onglet "plus" de la barre d'administration

![Onglet Plus](../img/restaurer/Onglet_Plus.PNG#printscreen#centrer)

* Choisir "Réutilisation de cours" dans le menu déroulant de cet onglet

![Réutilisation du cours](../img/restaurer/Reutilisation_du_cours.PNG#printscreen#centrer)

* Une fois dans la page "Réutilisation de cours", ouvrir le menu déroulant en haut à gauche (par défaut, il est ouvert sur "importation")

![Onglet menu déroulant](../img/restaurer/Onglet_menu_deroulant.PNG#printscreen#centrer)

* Choisir "Restauration"

![Menu déroulé](../img/restaurer/Menu_deroule.PNG#printscreen#centrer)

* Une fois sur la page "Restauration", glisser-déposer le fichier et cliquer sur l'onglet en bas en bleu foncé "RESTAURATION"

![Dépôt de la sauvegarde dans restauration](../img/restaurer/Depot_de_la_sauvegarde_dans_restauration.PNG#printscreen#centrer)

* Suivre chaque étape de la restauration en cliquant sur "CONTINUER" et "SUIVANT"


![Première page restauration](../img/restaurer/Premiere_page_restauration.PNG#printscreen#centrer)

![Fin première page restauration](../img/restaurer/Fin_premiere_page_restauration.PNG#printscreen#centrer)

## En vidéo

Si vous souhaitez avoir ce tutoriel en vidéo, vous pouvez retrouver l'instant Magistère du temps 5 de notre cycle d'accompagnement à la migration

<video width="640" height="360" controls="controls" type="video/mp4" preload="none" src="https://podeduc.apps.education.fr/media/videos/55a03ea3ddbdf24fb6752801418271fc02e9a3220f0c99784bed508a469bb389/65856/720p.mp4">Votre navigateur ne permet pas la lecture de la vidéo.</video>