# Sauvegarder un parcours 
Dans certains cas, il est nécessaire de sauvegarder un parcours pour l'utiliser dans une autre des plateformes de l'écosystème Élea - Magistère - Réseau des concepteurs, il faudra sauvegarder le parcours de la plateforme d'origine pour le restaurer sur la plateforme cible.  

:::info
L'accès au menu de sauvegarde nécessite de se trouver dans le parcours à sauvegarder et d'y avoir un rôle de formateur.
::: 

Cette opération comporte 3 étapes.

## Étape 1 - Accéder à la sauvegarde

* Dans le menu "Plus" du parcours à exporter, sélectionner "Réutilisation de cours"

![illustration étape 1 Réutilisation de cours](../img/sauvegarderparcours/etape_1.png#printscreen#centrer)


* Dans le menu situé à gauche, choisir "Sauvegarde".

![étape 2 Sauvegarde](../img/sauvegarderparcours/etape_2.png#printscreen#centrer)



## Étape 2 - Choisir les réglages de sauvegarde

* Choisir les réglages de la sauvegarde ainsi que les éléments du parcours à exporter. On peut exporter tout ou partie du parcours.

    * La page 1 permet de définir les paramètres généraux du parcours (fichiers, calendriers, badges, banque de question, groupes et groupements, ...)
    * La page 2 présente les différentes sections et activités à sauvegarder.
    * La page 3 est un récapitulatif de vos choix avant d'effectuer la sauvegarde.

![illustration étape 3 parties à exporter](../img/sauvegarderparcours/etape_3.png#printscreen#centrer)

* Vérifier les éléments exportés dans le récapitulatif de la sauvegarde.


![illustration étape 4 Récapitulatif](../img/sauvegarderparcours/etape_4.png#printscreen#centrer)


## Étape 3 - Effectuer et télécharger la sauvegarde

* Effectuer la sauvegarde.

![illustration étape 5](../img/sauvegarderparcours/etape_5.png#printscreen#centrer)

* Télécharger la sauvegarde sur votre ordinateur.

![illustration étape 5 bis](../img/sauvegarderparcours/etape_5_bis.png#printscreen#centrer)

:::info
Pour restaurer cette sauvegarde sur la plateforme cible, vous pouvez lire l'article [Restaurer un parcours](Restaurer%20un%20parcours.md)
:::
