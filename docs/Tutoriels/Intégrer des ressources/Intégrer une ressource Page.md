# Intégrer une ressource Page

**La ressource Page est un document HTML dans lequel vous pouvez ajouter du texte, des tableaux, des images, des hyperliens ou des fichiers multimédias.** A la différence de la ressource "[***Zone texte et média***]", le contenu d'une page ne s'affiche pas directement sur la page d'accueil du parcours ou une section, mais sur une page séparée, s'intégrant parfaitement dans la navigation du parcours.

:::success

**👍USAGES PÉDAGOGIQUES POSSIBLES** 


Selon la méthode [ABC Learning Design](https://dane.ac-reims.fr/dokiel/moodle/draganddrop_ABC.html), la ressource Page est étiquetée <img src="https://minio.apps.education.fr/codimd-prod/uploads/upload_8975e8e0e615281a73de873d6991d2f5.png" width="100"></img>. C'est un contenu informatif **multimédia** qui s'intègre idéalement dans la navigation d'un parcours scénarisé. 
Elle offre l'avantage de pouvoir être modifiée facilement tandis que la ressource "fichier" doit être modifiée hors ligne et déposée de nouveau pour être actualisée. 

:::

:::danger 
**❗POINTS DE VIGILANCE**

Le contenu de la page ne doit pas être trop long pour faciliter la navigation (boutons "suivant" et "précédent" situés au bas de la page).
:::


## Étape 1 : Créer une ressource Page

### Depuis la barre d'édition

1. Dans la barre d'édition du tiroir de droite, cliquer sur le menu **"Ressources"**.
2. Cliquer sur l'icône correspondant à la ressource **Page**.

![Menu édition page](../img/RessourcePage/02-menu_édition_page.png#printscreen#centrer)

3. Une nouvelle fenêtre s’ouvre pour configurer la ressource.

## Étape 2 : Paramétrer votre ressource Page

1. Commencer par attribuer un nom à la ressource 

![04-attribuer un nom](../img/RessourcePage/04-attribuer%20un%20nom.png#printscreen#centrer)

2. Entrer une **description** si nécessaire : 
 * Par défaut, elle n'apparaît pas dans le cours
 * Pour la faire apparaître sur la page d'accueil du parcours ou dans les sections, cochez la case "Afficher la description…"

![05-ajouter une description.png](../img/RessourcePage/05-ajouter%20une%20description.png#printscreen#centrer)

3. Ajouter du **contenu**, en insérant avec l'éditeur ce que l'on souhaite voir figurer sur la page : ce peut être du texte, des images, des liens, du contenu multimédia…

![06-ajouter du contenu.png](../img/RessourcePage/06-ajouter%20du%20contenu.png#printscreen#centrer)

4. Ajuster le paramétrage de votre ressource page (restriction d'accès, achèvement d'activité...) : Voir **Paramétrer des activités** : suivi d'achèvement et [restrictions d'accès](../Cr%C3%A9er%20et%20g%C3%A9rer%20des%20parcours/Restriction%20d%E2%80%99acc%C3%A8s.md)


5. Cliquer sur le bouton **"Enregistrer et afficher"** pour voir apparaître le contenu de votre page.
![07-enregistrer et afficher.png](../img/RessourcePage/07-enregistrer%20et%20afficher.png#printscreen#centrer)



