# Intégrer une ressource Livre
![IconeLivre.png](../img/RessourceLivre/IconeLivre.png#printscreen#centrer)

## Présentation
La ressource livre permet au formateur de créer une ressource composée de plusieurs pages appelées chapitres, comme un livre. Un sommaire est disponible à droite de l'écran. Les livres peuvent contenir du texte ainsi que des fichiers médias et sont utiles pour afficher des contenus longs à organiser en chapitres.

## Usages possibles
Un livre peut être utilisé pour :
* Proposer des contenus longs, à consulter :
    * en les organisant en chapitres
    * en alternant texte et médias
* Mettre à disposition un mode d'emploi
* Créer une FAQ


## Création de la ressource.
Celle-ci se fait en deux étapes. La première consiste à créer et configurer la ressource. La seconde permet de créer des chapitres (et sous-chapitres).
## Étape 1 : création et configuration
En mode édition, cliquer sur le lien « Ajouter une activité ou ressource » et dans l’onglet «Ressources» choisir Livre. Au delà des réglages communs à toutes les ressources, ne sont décrits ci-dessous que ceux spécifiques du livre.
### Apparence
Dans cette zone, les choix sont les suivants : 
* Le format des titres 
    * aucun
    * puces
    * nombres
    * indentation
* Le style de navigation
    * uniquement par la table des matières
    * par des images ou du texte (pour la navigation entre les chapitres)
* Titres personnalisés (permet de remplacer le titre du chapitre au dessus du texte par un texte éventuellement plus long)

![Livre-Param-Apparence.png](../img/RessourceLivre/Livre-Param-Apparence.png#printscreen#centrer)

## Étape 2 : création des chapitres
Une fois le paramétrage effectué, le formateur peut créer les chapitres du livre. Ceux-ci s’afficheront dans la colonne centrale et la table des matières en haut de la colonne de droite. Chaque chapitre est composé d’un titre (une case à coche permet de créer un sous-chapitre) et d’un contenu (texte, images, sons…).

### Le premier chapitre
Créer le premier chapitre en cliquant sur le bouton « accéder » de la ressource ou directement après la configuration. Une fois l’enregistrement du chapitre effectué, la table des matières apparaît.

![Livre-AjouterChapitreUn.png](../img/RessourceLivre/Livre-AjouterChapitreUn.png#printscreen#centrer)

### Et les suivants
Une fois le premier chapitre créé, il est possible de créer d'autres chapitres et sous-chapitres. Le chapitrage est limité à deux niveaux. L'illustration ci-dessous montre la création d'un sous-chapitre.

![Livre-AjouterAutreChapitre.png](../img/RessourceLivre/Livre-AjouterAutreChapitre.png#printscreen#centrer)

Pour créer un sous-chapitre il faut penser à cocher la case correspondante.
La table des matières se présente sous la forme suivante : 

![Livre-TableDesMatieres-ModeEdition.png](../img/RessourceLivre/Livre-TableDesMatière-ModeEdition.png#printscreen#centrer)

Toutes les entrées fonctionnent comme des liens. Veiller à mettre des titres courts car la largeur de la table est limitée.
En mode édition, au regard de chaque entrée du livre, les opérations suivantes sont disponibles : 

![LivreOutilsEdition.png](../img/RessourceLivre/Livre-OutilsEditionChapitre.png#printscreen#centrer)

* Déplacement du chapitre (flèche)
* Modification du contenu du chapitre (roue dentée)
* Suppression du chapitre (Corbeille)
* Cacher le chapitre aux participants (oeil)
* Ajout d'un chapitre (plus)

## Autres fonctionnalités du livre
* Il est possible pour les utilisateurs d'imprimer le chapitre qu'ils affichent ou d'imprimer le livre en entier via le menu "Plus" du livre
* Le formateur a la possiblité d'importer des chapitres (au format html) dans le livre
