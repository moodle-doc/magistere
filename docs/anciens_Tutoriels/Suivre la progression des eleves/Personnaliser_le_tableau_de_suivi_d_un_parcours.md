
Dans les parcours de la plateforme Éléa, lorsque vous ajoutez une nouvelle activité ou ressource, les conditions d'achèvement liées à celle-ci sont paramétrées avec une valeur par défaut. 

Par exemple, pour une ressource page, l'achèvement d'activité est réglée par défaut de la façon suivante :

![reglage_page_defaut](../img/Tuto_personnaliser_tableau_suivi/reglage_page_defaut.png)

Ce réglage a pour conséquence d'afficher cette **ressource page** dans le tableau de suivi et la carte de navigation si vous en avez ajouté une au début du parcours.

![tableau_suivi](../img/Tuto_personnaliser_tableau_suivi/tableau_suivi.png)

![carte_navigation](../img/Tuto_personnaliser_tableau_suivi/carte_navigation.png)

Pour un parcours très long, il peut être utile de **simplifier le tableau de suivi et la carte de navigation** de façon à ne faire apparaître que les étapes clés du parcours.

La méthode consiste alors à paramétrer **l'achèvement d'activité** en sélectionnant l'option **"ne pas afficher l'état d'achèvement"**.

![enlever_achevement](../img/Tuto_personnaliser_tableau_suivi/enlever_achevement.png)

En appliquant ce paramétrage sur notre ressource page, celle-ci n'apparaît plus dans le tableau de suivi et la carte de navigation.

![tableau_suivi2](../img/Tuto_personnaliser_tableau_suivi/tableau_suivi2.png)

![carte_navigation2](../img/Tuto_personnaliser_tableau_suivi/carte_navigation2.png)



# Exemple de simplification du parcours PSC1 :

Dans la vidéo suivante, nous vous proposons une méthode pour simplifier le tableau de suivi du parcours PSC1 hybride récupéré sur la éléathèque et qui comporte de nombreuses activités et ressources.

<iframe title="Plateforme Éléa - Simplifier le tableau de suivi du parcours PSC1 hybride" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/1ae63250-7273-48a7-a5e7-ffde5a79ac3b" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>


