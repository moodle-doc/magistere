# Intégrer une activité Wiki

<iframe title="e-éducation &amp; Éléa - Intégrer une activité Wiki dans un parcours" width="560" height="315" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/4113182a-7ae7-4a78-b62e-6fabd5a74121" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

Remarque : 

- Si vous souhaitez ajouter une autre page à votre wiki, indiquez le nom de celle-ci entre double crochet [[...]] sur la première page créée.

![ajouter une page à un wiki](../img/activite_wiki/activite_wiki1.png#printscreen#centrer)

- Puis enregistrez et afficher

![paramétrer une nouvelle page à un wiki](../img/activite_wiki/activite_wiki2.png#printscreen#centrer)

- Cliquez sur le nom de la page suivante (qui apparaît en rouge), une nouvelle fenêtre s'ouvre permettant la création d'une nouvelle page.

![créer une nouvelle page](../img/activite_wiki/activite_wiki3.png#printscreen#centrer)

- Vous pouvez désormais renseigner votre nouvelle page
![renseigner la nouvelle page à un wiki](../img/activite_wiki/activite_wiki4.png#printscreen#centrer)


Vous pouvez ajouter autant de pages que vous le souhaitez.

