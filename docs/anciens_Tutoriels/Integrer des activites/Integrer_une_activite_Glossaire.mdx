---
tags:
  - activité
  - glossaire
  - collaboratif
---
import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';


# Intégrer une activité Glossaire

**L'activité Glossaire** permet aux participants de créer et de gérer une liste de définitions,
comme un dictionnaire.

:::tip Usages pédagogiques possibles  
###### selon la méthode [ABC Learning Design](https://abc-ld.org/tool-wheel/)

![ABC](../img/ABC/vignette_ABC_acquisition.png#abc) : Le Glossaire alimenté par l'enseignant remplace les définitions du cahier ou du répertoire. Les définitions peuvent être accompagnées de documents multimédias (ex. images ou vidéos).

![ABC](../img/ABC/vignette_ABC_collaboration.png#abc) : Le Glossaire est alimenté par les élèves ; certains élèves peuvent être "valideurs".

![ABC](../img/ABC/vignette_ABC_enquete.png#abc) : Le Glossaire permet de collecter des informations.
:::

<details>
<summary> Tutoriel vidéo (ancienne version d'Éléa)</summary>

<iframe title="e-éducation &amp; Éléa - Intégrer une activité glossaire dans un parcours" width="560" height="315" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/db6faa39-467b-4c12-b28b-409674444004" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>
</details>

## Création de l'activité

Pour créer une activité **Glossaire**, ouvrez le parcours sur lequel vous souhaitez ajouter cette activité en mode édition. 

<Tabs queryString="action">
<TabItem value="Depuis la barre d'édition" label="Depuis la barre d'édition">

Dans la barre d'édition du tiroir de droite, cliquez sur le menu "Activités", puis sur l'icône correspondante à l'activité Glossaire.

![Icône glossaire](../img/activite_glossaire/glossaire0.png)

</TabItem>
<TabItem value="Depuis le sélecteur d'activités" label="Depuis le sélecteur d'activités">

- Cliquez sur **"Ajouter une activité ou une ressource"**;

- Sélectionnez l'activité **"Glossaire "** dans la liste ;  
![Icône glossaire](../img/activite_glossaire/glossaire0.png)

- Et enfin cliquez sur **"Ajouter"**.  

</TabItem>
</Tabs> 

Une nouvelle page s'ouvre pour définir les paramètres de l'activité.

## Intégrer l'activité 

### Généralités

1. Commencez par attribuer un nom à votre activité et si vous le souhaitez une petite description.

![généraux](../img/activite_glossaire/glossaire2.png#printscreen#centrer)

:::tip Type de **glossaire**
Le type de glossaire par défaut est "Secondaire" (Conseil : pour débuter, ne pas modifier)  
NB : dans un glossaire **principal**, les articles des glossaires secondaires peuvent être importés ; il ne peut y en avoir qu'un seul par cours.
:::

### Autres paramètres importants

2. **Articles** :
    - Approuvé automatiquement :  
    Si ce réglage est défini à non, les articles requièrent une approbation de la part de l'enseignant avant de pouvoir être consultés.
    - Toujours autoriser la modification :
    Ce réglage détermine si les étudiants pourront toujours modifier leur article, ou pas.
    - Autoriser les doublons :  
    Si ce réglage est activé, il sera possible d'avoir plusieurs articles pour le même nom de concept.
    - Autoriser les commentaires :  
    Si ce réglage est activé, tous les utilisateurs ayant l'autorisation de créer des commentaires pourront en ajouter aux articles.
    -  Activer les liens automatiques :  
    Il est possible d'activer la création automatique d'un lien : quand un terme du glossaire figure dans le cours, un lien permet d'afficher la définition dans une boite de dialogue.
     
![généraux](../img/activite_glossaire/glossaire3.png#printscreen#centrer)

3. Terminer la création du glossaire avec le bouton "**Enregistrer et revenir au cours**"

## Ajout des définitions

Les définitions peuvent être ajoutées par l'enseignant et/ou par les élèves.
1. Cliquez sur l'activité pour l'ouvrir. 

![activité](../img/activite_glossaire/glossaire4.png#printscreen#centrer)

2. L'enseignant / l'élève clique sur le bouton "**Ajouter un nouvel article**".

![activité](../img/activite_glossaire/glossaire5.png#printscreen#centrer)

### A compléter obligatoirement :
3. L'enseignant / l'élève complète :
    - Le **concept** (Terme à définir)
    - La **définition** (avec la possibilité de rajouter une image, un son, une vidéo ou un document annexe)

![activité](../img/activite_glossaire/glossaire6.png#printscreen#centrer)

### A regarder :
4. Il est possible de rajouter :
    - Des **termes associés** (mots qui appellent la définition)
    - Des **annexes** (documents joints)
5. **Liaison automatique** : Si la case "**Article lié automatiquement**" est cochée, à chaque occurrence du terme dans le cours, un lien permet d'afficher une boite de dialogue avec la définition.
6. L'enseignant/l'élève clique sur le bouton "**Enregistrer**"

![activité](../img/activite_glossaire/glossaire6bis.png#printscreen#centrer)

:::tip  Bon à savoir
- Si vous avez défini le paramètre "**Approuvé automatiquement**" sur NON, l'enseignant doit approuver la définition avant que celle-ci soit consultable par tous.

![activité](../img/activite_glossaire/glossaire7.png#printscreen#centrer)

- Il est possible de **déléguer l'approbation des définitions** à un élève :
 * Cliquez sur le menu "**Plus > Rôles attribués localement**"
 * Sélectionnez le rôle "**Enseignant**"
 * **Ajoutez les élèves** qui auront ce rôle dans le glossaire

![activité](../img/activite_glossaire/glossaire8.png#printscreen#centrer)
:::

:::danger Point de vigilance
Les **définitions d'un glossaire** ne sont pas comprises dans le fichier de sauvegarde d'un parcours Éléa, comme toutes les traces des activités d'élèves.  
**Il faut donc exporter vos définitions** : c'est un fichier .xml que vous pourrez ensuite importer dans un cours.
:::

