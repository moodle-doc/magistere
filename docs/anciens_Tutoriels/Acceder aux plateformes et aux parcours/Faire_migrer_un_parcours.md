# Faire migrer un parcours sur une autre plateforme

Je viens de finir mon parcours. Je souhaite : 
- le transférer sur communaute.elea pour travailler avec d'autres collègues.
- le récupérer sur la plateforme de mon bassin pour le proposer à mes élèves


## Etape 1 : Sur la plateforme où se trouve le parcours
__Prérequis__ : Avoir les droits d'enseignants sur le parcours à migrer

Connectez-vous à la plateforme Eléa où se trouve le parcours à migrer. Ouvrez le parcours. 
Dans la barre de menu située en haut du parcours,
![admin-icon](../img/migrer_un_parcours/capture0.png#printscreen#centrer)

dépliez le menu "Plus", puis cliquez sur "Réutilisation de cours".

![admin-icon](../img/migrer_un_parcours/capture1.png#printscreen#centrer)

Pour finir, cliquez sur Importation, puis sélectionnez "Sauvegarde" 

![admin-icon](../img/migrer_un_parcours/capture2.png#printscreen#centrer)

Il s'agit maintenant de choisir les données qui seront sauvegardées. Une fois les éléments inclus, cliquez sur "suivant"  
Remarque : Il n'est pas possible actuellement d'inclure les badges. Il conviendra de les rajouter manuellement.

Il vous est également possible de ne sauvegarder qu'une partie du parcours. Pour cela, il vous suffit de décocher les éléments pour qu'ils ne figurent pas dans la sauvegarde.

![admin-icon](../img/migrer_un_parcours/capture2a.png#printscreen#centrer)

Cliquez sur "suivant" puis "Effectuer la sauvegarde" et pour finir "Continuer"

![admin-icon](../img/migrer_un_parcours/capture05.png#printscreen#centrer)

Dans la zone de sauvegarde, vous pouvez télécharger votre fichier de sauvegarde sur votre ordinateur. Gardez le précieusement. Il vous servira pour l'étape 2.
 ![admin-icon](../img/migrer_un_parcours/capture2aa.png#printscreen#centrer)


## Etape 2 : Sur la plateforme où vous souhaitez migrer le parcours

Créer un nouveau parcours en utilisant **impérativement** le gabarit "parcours vide" !

![admin-icon](../img/migrer_un_parcours/parcours_vide.png#printscreen#centrer)

![admin-icon](../img/migrer_un_parcours/parcours_vide2.png#printscreen#centrer)

Rendez-vous dans ce nouveau parcours

Dans la barre de menu située en haut du parcours,
![admin-icon](../img/migrer_un_parcours/capture0.png#printscreen#centrer)

dépliez le menu "Plus", puis cliquez sur "Réutilisation de cours".
![admin-icon](../img/migrer_un_parcours/capture1.png#printscreen#centrer)

Pour finir, cliquez sur Importation, puis sélectionnez "Restauration" 
![admin-icon](../img/migrer_un_parcours/capture3.png#printscreen#centrer)

Importez le fichier de sauvegarde de votre parcours. Vous avez deux possibilités :


- Faire un glisser votre fichier de sauvegarde dans la zone à cet effet

![admin-icon](../img/migrer_un_parcours/capture4.png#printscreen#centrer)

- Cliquez sur le bouton "Choisir un fichier" et sélectionnez dans votre ordinateur la sauvegarde que vous venez de faire de votre parcours.

![admin-icon](../img/migrer_un_parcours/capture5.png#printscreen#centrer)

Il ne vous reste plus qu'à cliquer sur "Continuer".

![admin-icon](../img/migrer_un_parcours/capture6.png#printscreen#centrer)

Cliquez en bas à droite sur "Continuer".

Cochez **impérativement** "Fusionner le cours sauvegardé avec ce cours". Cliquez sur "Continuer".

![admin-icon](../img/migrer_un_parcours/capture7.png#printscreen#centrer)

Lors de cette phase de la restauration, il vous est possible de :
- renommer votre cours, 
- de définir une date de début du cours
- de conserver les groupes

![admin-icon](../img/migrer_un_parcours/capture8.png#printscreen#centrer)

- De restaurer qu'une partie du parcours sauvegardé

![admin-icon](../img/migrer_un_parcours/capture9.png#printscreen#centrer)

Cliquez sur "Effectuer la restauration" puis sur "Continuer" à 2 reprises.

![admin-icon](../img/migrer_un_parcours/capture10.png#printscreen#centrer)

![admin-icon](../img/migrer_un_parcours/capture11.png#printscreen#centrer)

Attention : si le parcours modèle comprend des badges, il convient de les [ajouter](https://dne-elearning.gitlab.io/moodle-elea/documentation/docs/Tutoriels/Concevoir%20des%20parcours/Ajouter_un_badge/ "tutoriel pour ajouter un badge") manuellement.







