---
tags:
  - Inscription
  - groupe
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';


# Inscrire à un parcours Éléa un groupe d'élèves issus de classes différentes

<details>
<summary> Tutoriel vidéo (ancienne version d'Éléa)</summary>

<iframe title="e-éducation &amp; Éléa - Inscrire des groupes d'élèves à un parcours" width="560" height="315" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/404926fd-4d91-4673-9684-be3072365cdd" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

</details>

Il est fréquent que pour des raisons d'organisation des élèves issus de classes différentes forment un groupe pour suivre un cours donné, participer à un projet établissement, etc.

Dans de nombreux cas, vous trouverez ces groupes avec les classes et pourrez inscrire directement vos élèves, mais dans certains cas particuliers (pour certains ENT, pour des groupes qui ne sont pas liés à l'emploi du temps) ils sont absents.

Que faire toutefois si ce groupe inter-classes ne figure pas parmi les cohortes proposées ?

Il est bien entendu possible d'inscrire au parcours les classes entières dont sont issus les élèves du groupe, quitte donc à consentir que des élèves supplémentaires aient eux-aussi accès au parcours. Si en revanche on souhaite strictement donner accès au parcours aux seuls élèves du groupe, deux scénarios sont à envisager.

## Premier scénario : un groupe de petit effectif

Si le groupe compte peu d'élèves, on procèdera à l'inscription manuelle de chaque élève concerné.

Dans le tableau de bord du professeur, cliquez sur l'icone qui donne accès à la liste des utilisateurs du parcours.

![Liste des utilisateurs](../img/images_Tutoriel_Groupes_Classes_Différentes/Groupes_Classes_Différentes01listedesutilisateurs.png#printscreen#centrer)

Puis cliquez sur "**Inscrire des utilisateurs**".

![Bouton Inscrire de nouveaux utilisateurs](../img/images_Tutoriel_Groupes_Classes_Différentes/Groupes_Classes_Différentes02inscriredenouveauxutilisateurs.png#printscreen#centrer)

Dans la fenêtre surgissante suivante, vous allez chercher un à un les élèves faisant partie du groupe.

![Fenêtre d'inscription](../img/images_Tutoriel_Groupes_Classes_Différentes/Groupes_Classes_Différentes03fenetreinscriptioneleves.png#printscreen#centrer)

1. Entrez le nom de famille d'un premier élève : repérez alors cet élève dans la liste déroulante qui apparaît et sélectionnez-le.

2. Vérifiez que le rôle "**Étudiant**" est bien sélectionné (rôle indiqué par défaut).

**Remarque** : si vous modifiez accidentellement le rôle attribué à l'élève à cette étape, il vous sera toujours possible de rectifier ultérieurement ce rôle en cliquant dans la liste des utilisateurs sur l'icone "**Attribution des rôles**"![Icône d'attribution des rôles](../img/images_Tutoriel_Groupes_Classes_Différentes/Groupes_Classes_Différentes04attributiondesroles.png#printscreen#centrer), puis en sélectionnant "**Étudiant**".

3. Cliquez sur "**Inscrire les utilisateurs et cohortes sélectionnés**".

4. Répétez ces opérations pour chaque élève du groupe devant être inscrit au parcours.

## Deuxième scénario

Dans certains cas, avec un effectif de groupe important, il sera plus simple d'inscrire d'abord la totalité des classes dont sont issus les élèves souhaités, pour ensuite **désinscrire** un à un les autres élèves de ces classes - élèves dont on ne souhaite donc pas qu'ils aient accès au parcours concerné.

Dans le tableau de bord du professeur, cliquez sur l'icone qui donne accès à la liste des utilisateurs du parcours.

![Liste des utilisateurs](../img/images_Tutoriel_Groupes_Classes_Différentes/Groupes_Classes_Différentes01listedesutilisateurs.png#printscreen#centrer)

Puis cliquez sur "**Inscrire des utilisateurs**".

![Bouton Inscrire de nouveaux utilisateurs](../img/images_Tutoriel_Groupes_Classes_Différentes/Groupes_Classes_Différentes02inscriredenouveauxutilisateurs.png#printscreen#centrer)

Dans la fenêtre surgissante suivante, vous allez chercher une à une les classes dont font partie les élèves du groupe considéré.

![Fenêtre d'inscription des cohortes](../img/images_Tutoriel_Groupes_Classes_Différentes/Groupes_Classes_Différentes05fenetreinscriptioncohortes.png#printscreen#centrer)

1. Vous pouvez :
   - Soit directement saisir le libellé d'une classe (ici appelée "cohorte") ;
   - Ou bien faire défiler en cliquant sur la flèche ▼ la liste des classes disponibles pour y sélectionner la classe souhaitée.
2. Vérifiez que le rôle "**Étudiant**" est bien sélectionné (rôle indiqué par défaut).
3. Cliquez sur "**Inscrire les utilisateurs et cohortes sélectionnés**".
4. Répétez ces opérations pour chacune des classes des élèves du groupe qui doivent être inscrits au parcours.

Vous allez ensuite désinscrire les élèves qui n'appartiennent pas au groupe voulu.

![Désinscription](../img/images_Tutoriel_Groupes_Classes_Différentes/Groupes_Classes_Différentes06desinscription.png#printscreen#centrer)

Vous pouvez :

1. Soit cliquer sur la croix **x** de désinscription en vis-à-vis de chaque élève ne faisant pas partie du groupe prévu.

2. Ou bien, cocher une à une chaque ligne des élèves à désinscrire ;

3. Puis choisir l'action "**Supprimer les inscriptions sélectionnées**" dans la section "**Inscriptions Manuelles**" du menu déroulant, pour simultanément désinscrire tous les élèves sélectionnés.

## Lier des Parcours

Une fois que les élèves d'un groupe ont été inscrits à un premier parcours (suivant l'un ou l'autre des deux scénarios précédents), il serait fastidieux de réitérer toutes ces opérations pour tout autre parcours auquel doit accéder ce même groupe. 

Il existe donc la possibilité de **lier** les utilisateurs entre deux parcours : c'est-à-dire de considérer un premier parcours comme étant **le parcours de référence**, car c'est le parcours qui a déjà comme inscrits les élèves du groupe souhaité ; puis, d'attribuer exactement cette même liste d'inscrits à un autre parcours.

Dans cet exemple, on considérera que le groupe considéré a été inscrit à un premier parcours.

![Lier deux parcours](../img/images_Tutoriel_Groupes_Classes_Différentes/Groupes_Classes_Différentes07lierparcours.png#printscreen#centrer)

Pour le deuxième parcours auquel doit prendre part ce même groupe, cliquez sur l'icone "**Lier aux utilisateurs d'un autre parcours**".

![Parcours de référence](../img/images_Tutoriel_Groupes_Classes_Différentes/Groupes_Classes_Différentes08parcoursreference.png#printscreen#centrer)

Dans la fenêtre surgissante suivante cliquez alors sur le premier parcours, et patientez jusqu'à ce que ce parcours vire au vert.

![Parcours de référence](../img/images_Tutoriel_Groupes_Classes_Différentes/Groupes_Classes_Différentes09parcoursreference.png#printscreen#centrer)

Vous pouvez alors fermer cette fenêtre avec la croix **X**.

Désormais la liste des élèves de ce premier parcours est ajoutée à celle de ce deuxième parcours.

Toute modification dans la liste des utilisateurs du premier parcours sera automatiquement répercutée dans celle du second parcours.

En revanche, toute modification de la liste des utilisateurs du second parcours n'affectera pas la liste du premier qui a servi ici de parcours de référence.
