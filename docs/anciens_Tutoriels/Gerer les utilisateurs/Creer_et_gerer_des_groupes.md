# Créer et utiliser des groupes

<iframe title="e-éducation &amp; Éléa - Créer et utiliser des groupes sur Éléa" width="560" height="315" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/f95c1548-b210-45c5-b505-714e6a73d4cc" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

**Attention** : Désormais, des groupes correspondant aux différentes classes sont automatiquement créés à chaque inscription d'une classe à un parcours.

![inscription des classes](../img/groupes/inscription_classes.png#printscreen#centrer)

Par défaut, sur un parcours, il existe donc des groupes séparés correspondant à chacune des classes inscrites au parcours. 

![groupes classes par défaut](../img/groupes/groupes_classes.png#printscreen#centrer)

Vous pouvez filtrer les résultats des participants par groupe-classe 

![groupes classes par défaut](../img/groupes/suivi_eleves.png#printscreen#centrer)



Par conséquent, pour chaque activité du parcours, dans les réglages courants, l'option "groupe séparés" est activée par défaut. 

![groupes classes par défaut](../img/groupes/groupes_separes.png#printscreen#centrer)

Vous pouvez modifier cette option si vous le désirez

![groupes classes par défaut](../img/groupes/groupes_separes2.png#printscreen#centrer)


