# Intégrer une vidéo

Sur la plateforme Éléa, il est déconseillé de déposer des vidéos directement dans un parcours au risque de dépasser rapidement la taille limite du parcours fixée à **250 Mo**. En effet, dépasser cette taille empêche la restauration du parcours en cas de sauvegarde.

Il est donc préférable de stocker les vidéos sur une plateforme de streaming telle que Youtube ou mieux, les plateformes institutionnelles nationales [Portail Tubes](https://tubes.apps.education.fr/#) ou [PodEuc](https://podeduc.apps.education.fr/) d'apps éducation, qui ne contiennent aucune publicité.

Dès lors, vous pourrez facilement intégrer les vidéos hébergées sur ces plateformes dédiées dans **l'éditeur de texte d'Éléa**. Cet éditeur "Atto" est présent dans de nombreuses ressources et activités d'Éléa : Page, Étiquette, QCM, Appariement, Test, etc...

Activez le mode **édition** sur votre parcours, puis éditez votre ressource ou activité.

## Intégration directe à partir d'un lien

Pour les vidéos hébergées sur PodEduc, Tubes, ou Youtube, dans l'éditeur Atto, vous pouvez cliquer sur le bouton "Insérer un lien".

![Editeur atto](../img/integrer_video/Capture9.png#printscreen#centrer)

Copier/coller directement le lien de la vidéo, puis cliquer sur le bouton "Créer un lien".

Lorsque vous Enregistrer votre ressource ou activité, le lien sera automatiquement converti en vidéo.

## Intégration via le code "iFrame" de la vidéo

Sur les plateformes de streaming, il est souvent possible de récupérer un code d'intégration HTML nommé iframe. Ce code ressemble à ceci `<iframe src="https://podeduc.apps.education.fr/video ... </iframe>`.

Sur Youtube, ce code est accessible via le bouton "Partager" situé sous la vidéo, bouton intégrer.

![Editeur atto](../img/integrer_video/Capture6.png#printscreen#centrer)

Sur Portail Tubes et PodEduc, le code est accessible via le bouton ci-dessous.

![Editeur atto](../img/integrer_video/Capture7.png#printscreen#centrer)

Une fois le code copié, il faut l'insérer dans l'éditeur de texte Éléa en mode HTML. 
Ce mode est disponible en dépliant le menu en cliquant sur le bouton représentant une flèche vers le bas. Une extension de la barre de menu apparaît en dessous et sur cette deuxième ligne, vous trouverez le bouton `</>` qui permet de basculer le texte en mode HTML.

![Editeur atto](../img/integrer_video/Capture8.png#printscreen#centrer)

Il ne vous reste qu'à coller le code iframe, puis, en recliquant sur le bouton `</>` votre vidéo devrait apparaître dans l'éditeur de texte.
En personnalisant le code HTML, il est possible de modifier la taille de la vidéo (width="560" height="315" ), de démarrer la vidéo à partir d'un temps déterminé (en ajoutant après le ? start=4s), etc.

## Intégration via le bouton "insérer vidéo" de l'éditeur

Dans l'éditeur Atto, vous pouvez cliquer sur le bouton "Insérer ou modifier un fichier audio/vidéo".

![Editeur atto](../img/integrer_video/Capture1.png#printscreen#centrer)

- Pour une vidéo Youtube, il est possible de coller directement le lien dans l'onglet le champ "URL Source" (onglet "Lien").

![Editeur atto](../img/integrer_video/Capture2.png#printscreen#centrer)

- Pour une vidéo Tubes, il faut récupérer le lien de téléchargement de la vidéo. Connectez-vous au [Portail Tubes](https://tubes.apps.education.fr/#) et sélectionnez la vidéo que vous voulez intégrer.
Sous la vidéo, cliquez sur les "...", puis cliquez sur Télécharger.

![Editeur atto](../img/integrer_video/Capture3.png#printscreen#centrer)

Choisissez la qualité vidéo qui vous convient (720p est un bon compromis), puis copiez le lien de la vidéo en cliquant sur le bouton COPY.

![Editeur atto](../img/integrer_video/Capture4.png#printscreen#centrer)

Dans la fenêtre d'insertion média, cliquez sur l'onglet "Vidéo", puis collez l'URL de la vidéo dans le champ "URL Source vidéo".

![Editeur atto](../img/integrer_video/Capture5.png#printscreen#centrer)

L'intérêt de passer par cette méthode est de pouvoir personnaliser facilement la vidéo (taille et vignette dans options d'affichage, lecture automatique, boucle dans réglages avancés et même des sous-titres, légendes, chapitres à condition d'avoir les fichiers adéquates).

- Pour une vidéo PodEduc, la démarche est la même que précédemment, mais il faudra également le lien de téléchargement de la vidéo et pas le lien de la vidéo elle même (disponible uniquement sur des vidéos déposées par vous même).


