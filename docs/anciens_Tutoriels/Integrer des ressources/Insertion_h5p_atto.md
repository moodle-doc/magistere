# Insertion d'une ressource H5P dans l'éditeur de texte

La barre d'édition de "l'éditeur Atto" présent dans la plupart des activités et ressources permet désormais d'insérer un fichier H5P à partir d'une URL ou d'un H5P créé dans la banque de contenus.

![Barre menu](../img/Insertion_H5p/Capture1.png)

Il est préférable de n'insérer des fichiers H5P qui ne nécessitent pas de suivi, donc plutôt des H5P de type "ressources interactives".

Par exemple, dans une ressource page, on peut insérer un diaporama interactif (course presentation), ou un texte accordéon (accordion), un livre interactif (interactive book), une galerie interactive d'images (Image slider), une frise chronologique (Timeline), une visite virtuelle (virtual tour), cette liste est non exhaustive...

Pour les autres activités H5P de type exerciseurs, il vaudra mieux insérer les H5P en tant qu'activité dans le parcours pour pouvoir avoir un retour des résultats des élèves.

## Exemple d'insertion d'un accordéon dans une étiquette :

Prérequis : Avoir créé un fichier H5P de type "accordion" dans la banque de contenus.

Pour cela, dans votre parcours, cliquez sur le menu sandwitch, puis "Administration du cours", puis "Banque de contenus"

![Barre menu](../img/Insertion_H5p/Capture2.png)

![Barre menu](../img/Insertion_H5p/Capture3.png)

Dans la banque de contenus, créez votre fichier H5P et enregistrez le.

Pour davantage de précisions, consultez le tutoriel suivant : [tutoriel](https://dne-elearning.gitlab.io/moodle-elea/documentation/docs/Tutoriels/Integrer%20des%20contenus%20interactifs/Banque_de_contenus_H5P/)

- Insérer une étiquette d'accueil au début du parcours.

- En mode édition, cliquez dans le menu Atto sur le bouton H5P puis, dans la fenêtre qui s'ouvre, cliquez sur "Parcourir les dépots.

![Menu H5P](../img/Insertion_H5p/Capture4.png)

Insérez ensuite votre fichier H5P à partir de la banque de contenus.

![Banque de contenus](../img/Insertion_H5p/Capture5.png)

Enregistrez votre étiquette et visualisez votre ressource interactive.

