# Accordion H5P (Menu déroulant)

Le contenu interactif **Menu déroulant** permet d’intégrer un texte sous forme d’une série de "blocs" de texte emboîtés et déroulables les uns sous les autres par un simple clic sur le bloc souhaité. 

Le texte est ainsi structuré en différentes sections empilées verticalement, sections que l’utilisateur dépliera à la manière d’un "Menu déroulant" pour accéder progressivement aux différents contenus (à raison d'une seule section affiché à la fois). 

**Exemple** : la deuxième section du **Menu déroulant** est ici dépliée.

![Exemple d'accordéon](../img/accordionaccordeon/accordionaccordeon01exemple.png#printscreen#centrer)

L’activité **Menu déroulant** est donc un bon moyen d’offrir une présentation interactive pour des textes. 

Et cette présentation est particulièrement adaptée pour des textes longs car ils seront ainsi d’un accès plus attractif en étant organisés en sections cliquables une à une.

**Exemple** : la troisième section du **Menu déroulant** est ici dépliée.

![accordionaccordeon02exemple](../img/accordionaccordeon/accordionaccordeon02exemple.png#printscreen#centrer)

**Remarque** : Seul du texte peut être intégré comme contenu dans un tel **Menu déroulant** (mais pas d’images ou de vidéos). Ce texte peut toutefois contenir des liens internet cliquables.

Nous commencerons par créer un contenu interactif du type (en anglais) "**Accordion**".

![Icone de l'activité accordéon](../img/accordionaccordeon/accordionaccordeonicone.png#printscreen#centrer)

## Ajouter le contenu interactif

1. Activez le mode édition dans votre parcours en cliquant sur ce bouton en haut à droite.

2. Cliquez dans la section souhaitée de votre parcours sur " **Ajouter une activité et ressource** " pour y créer une activité du type "**Contenu Interactif**".

![Bouton H5P](../img/creercontenusinteractifspartiecommune/H5Pn.png#printscreen#centrer)

Ou créez l'activité à partir de la banque de contenus.

![Bouton H5P](../img/creercontenusinteractifspartiecommune/H5Pb.png#printscreen#centrer)

3. Dans le formulaire qui s’affiche, après avoir saisi éventuellement une rapide description de l'activité qui va être créée, repérez le type de contenu interactif souhaité dans la section "**Éditeur**" et le menu "**H5P hub - Sélectionnez le type d'activité**" ; puis cliquez sur le bandeau correspondant pour créer la ressource voulue.

Si vous souhaitez davantage d'informations sur la procédure de création d'un contenu interactif, un tutoriel détaillé est disponible ici : [Tutoriel Créer un Contenu Interactif](https://dne-elearning.gitlab.io/moodle-elea/documentation/docs/Tutoriels/Integrer%20des%20contenus%20interactifs/Creer_Contenus_Interactifs/).

## Créer la première section du Menu déroulant

![Interface d'édition de l'activité Menu déroulant](../img/accordionaccordeon/accordionaccordeon03interface.png#printscreen#centrer)

1. Donnez un titre au **Menu déroulant** : c'est sous ce titre que l'activité apparaîtra dans le parcours Éléa.
2. Renseignez le "**Titre**" de la première section de texte qui apparaitra en haut du **Menu déroulant**.
3. "**Texte**" : saisissez ici le contenu de cette première section. Vous disposez dans la barre d’outils des outils usuels d'un éditeur de texte : mise en gras ou en italique, disposition centrée, à droite ou à gauche des paragraphes, insertion/suppression de liens hypertextes ou de lignes horizontales de séparation, choix des styles et tailles de polices de caractères, ainsi que des couleurs d'écriture ou de fond.
4. **Remarque** : cliquez-tirez sur la flèche en bas à droite de l'éditeur de texte permet de redimensionner celui-ci pour un meilleur confort de travail.

## Ajouter et organiser les sections du Menu déroulant

![Icônes de navigation dans le menu déroulant](../img/accordionaccordeon/accordionaccordeon04interface.png#printscreen#centrer)

Le texte du **Menu déroulant** va donc être organisé en différentes sections (appelées ici "**panels**"). 

1. Cliquez que bouton "**AJOUTER PANEL**" pour créer la section suivante du **Menu déroulant**.

![Le bouton ajouter "panel"](../img/accordionaccordeon/accordionaccordeon05boutonajouterpanel.png#printscreen#centrer)

Chaque section comportera un titre distinctif : l’utilisateur cliquera sur ce titre de section pour la faire apparaître en déroulé et pouvoir ainsi la lire (la précédente section qui avait été "ouverte" se repliera alors automatiquement de son côté).

2. Lors de la création du **Menu déroulant**, pour votre confort de lecture à l'écran, il vous est possible de condenser les informations (titre et contenu) d'une section donnée en cliquant sur la flèche blanche **▼** qui est située dans la barre bleue de titre. Ultérieurement, cliquez sur la flèche ► pour redéplier les informations d'une section afin de pouvoir à nouveau les modifier.
3. Les sections du **menu déroulant** peuvent être déplacées entre elles (montées dans la liste en cliquant à droite sur **▲**, ou descendues en cliquant sur **▼**) ce qui modifiera leur ordre d'empilement vertical dans le **Menu déroulant**. Cliquer sur la croix **x** permet de supprimer entièrement une section donnée.

Votre **Menu déroulant** est prêt : vous pouvez cliquer en bas de page sur "**ENREGISTRER ET AFFICHER**" pour le parcourir.

![Le bouton enregistrer et afficher](../img/accordionaccordeon/accordionaccordeon06boutonenregistreretafficher.png#printscreen#centrer)

(Crédits textes : WIKIPEDIA)
