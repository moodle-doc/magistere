# Créer un contenu interactif H5P

Le module "**Contenu Interactif (H5P)**" permet d'accéder à une collection d'activités diverses, pouvant mêler textes, images, vidéos et sons, et offrant toute une gamme d'interactions variées entre l'élève et les contenus créés. 

L'élève recevra des rétroactions selon ses résultats. Résultats qui seront automatiquement reportés dans le carnet de notes de la plateforme Éléa.

## Ajouter le contenu interactif

1. Activez le mode édition dans votre parcours en cliquant sur ce bouton en haut à droite ;

![Bouton pour basculer en mode édition](../img/creercontenuinteractif/modeedition.png#printscreen#centrer)

2. Dans la section souhaitée, cliquez sur "**Ajouter une activité ou une ressource**" ;

3. Dans la plateforme Éléa, 2 boutons de couleurs différentes permettent d'ajouter une activité H5P.

![Menu des activités en mode simple](../img/creercontenuinteractif/Capture1.png#printscreen#centrer)

- L'activité H5P bleue vous permet d'insérer un fichier externe (attention à la compatibilité selon les bibliothèques utilisées) ou un fichier provenant de la **banque de contenus** se trouvant dans Éléa.

- L'activité H5P noire vous permet d'insérer directement une activité H5P en créant cette activité ou en l'important à partir d'un fichier externe.

## Ajouter une activité H5P bleue

Vous devez d'abord renseigner le titre de l'activité et éventuellement une petite description.

![Menu des activités en mode simple](../img/creercontenuinteractif/Capture2.png#printscreen#centrer)

Vous devez **cliquez** ensuite dans la zone permettant de déposer un fichier.

(NB: Un lien vers la banque de contenus est disponible en dessous et vous permet d'afficher la banque de contenus dans un nouvel onglet.)

![ecran4](../img/banque_de_contenus_H5P/fenetre4.png#printscreen#centrer)

Une fenêtre s'affiche vous permettant de sélectionner la banque de contenus et les fichiers qu'elle contient.

Voir ce tutoriel pour créer une nouvelle activité dans la banque de contenus : [gérer la banque de contenus](https://dne-elearning.gitlab.io/moodle-elea/documentation/docs/Tutoriels/Integrer%20des%20contenus%20interactifs/Banque_de_contenus_H5P/)

![ecran5](../img/banque_de_contenus_H5P/fenetre5.png#printscreen#centrer)

Dans cette fenêtre, les 2 modes d'affichage présents dans la banque de contenus sont disponibles (icônes et détail des fichiers). Un autre bouton **"Afficher le dossier sous la forme d'arbre de fichiers"** permet d'afficher **l'ensemble des parcours** dans lequel vous êtes inscrit, et vous permet de sélectionner **les banques de contenus** d'autres parcours pour réutiliser un H5P disponible dans un autre parcours.

![ecran6](../img/banque_de_contenus_H5P/fenetre6.png#printscreen#centrer)

Finalisez les paramétrages de l'activité si nécessaire (options, achèvement, restriction, ...) comme sur toute autre activité de la plateforme Éléa. Une option supplémentaire H5P vous permet d'autoriser le téléchargement, l'intégration ou d'afficher un copyright.

![ecran7](../img/banque_de_contenus_H5P/fenetre7.png#printscreen#centrer)

Pour tester votre contenu interactif, cliquez en bas de page sur "Enregistrer et afficher".


## Ajouter une activité H5P noire

Sélectionner d'abord l'activité souhaitée.

Commencez par saisir une rapide description de l'activité qui va être créée.

![Champ de saisie pour la description de l'activité](../img/creercontenuinteractif/champ-description.png#printscreen#centrer)


Il y a ensuite **quatre** manières différentes d'accéder au type d'activité souhaité dans la section "**Éditeur**".

![Interface du menu H5P](../img/creercontenuinteractif/interfaceH5P.png )

Au choix (voir numéro correspondant sur illustration) :

1. Si vous connaissez déjà le nom (en anglais) du type d'activité cherché saisissez le directement dans la barre de recherche ; après avoir frappé "**Entrée**" cette activité apparaîtra directement en haut de liste.

2. Cliquez sur "**Utilisés récemment en premier**". Tous les contenus interactifs disponibles seront listés en fonction de votre dernière date d'utilisation de chacun entre eux.

3. La collection H5P de contenus interactifs s'enrichit régulièrement de nouvelles activités. Cliquez sur "**Plus récents en premier**" et tous les contenus interactifs disponibles seront listés en fonction de leur date de parution.

4. Cliquez sur "**A à Z**" et tous les contenus interactifs disponibles seront listés par ordre alphabétique (dénominations en anglais).

   

   Cliquez enfin sur le bandeau de cette activité pour la créer.

## Créer l'activité

Selon le type d'activité élaborée, vous pouvez consulter sur la plateforme Éléa le tutoriel correspondant qui vous détaillera pas à pas la démarche à suivre.

Pour tester votre contenu interactif, cliquez en bas de page sur "**Enregistrer et afficher**".

![Bouton enregistrer et afficher et options diverses](../img/creercontenuinteractif/afficher.png#printscreen#centrer)

**Remarque** : en option, une fois l'activité terminée, il est encore possible en bas de page, comme pour toute autre activité créée sur la plateforme Éléa, de fixer divers paramètres de notation, d'accès et d'achèvement pour cette activité.
