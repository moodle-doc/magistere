# Find Multiple Hotspots H5P (Image réactive à points multiples)

Ce contenu interactif se présente sous la forme d'une simple image d'arrière-plan : l'élève est invité par la consigne à y observer et déterminer certains éléments "clefs" sur lesquels il faudra cliquer. 

![Exemple d'activité de Find Multiple Hotspsots Image réactive à points multiples](../img/findmultiplehotspotsimageacliquer/findmultiplehotspotsimageacliquer01exemplefindmultiplehotspotsimageacliquer.png#printscreen#centrer)

Dans cet exemple l'élève doit cliquer sur les drapeaux des pays qui ne sont pas membres de l'U.E.

![Exemple d'activité de Find Multiple Hotspsots Image réactive à points multiples](../img/findmultiplehotspotsimageacliquer/findmultiplehotspotsimageacliquer02exemplefindmultiplehotspotsimageacliquer.png#printscreen#centrer)

La diversité des supports utilisables (schémas, photographies, illustrations variées) fait de ce contenu interactif une activité très polyvalente qui favorise de manière générale l'observation bien sûr, mais qui pourra par exemple amener l'élève à devoir effectuer des comparaisons entre divers éléments pour en déterminer des propriétés caractéristiques, à établir des contrastes ou à déterminer des points communs, ou bien encore à détecter des erreurs dans une image donnée.

La création d'une telle activité s'effectue en deux temps :

- On téléverse d'abord l'image d'arrière-plan.
- Puis, au-dessus de cette image, on trace des "zones réactives", c'est-à-dire des zones qui resteront invisibles de l'élève mais réagiront si on clique dessus en validant ou invalidant ce clic comme étant une bonne ou mauvaise réponse.

Nous commencerons par créer un contenu interactif de type (en anglais) "**Find Multiple Hotspots**".

![Icône de l'activité Find Multiple Hotspots Image réactive à points multiples](../img/findmultiplehotspotsimageacliquer/findmultiplehotspotsimageacliquericone.png#printscreen#centrer)

## Ajouter le contenu interactif

1. Activez le mode édition dans votre parcours en cliquant sur ce bouton en haut à droite.

2. Cliquez dans la section souhaitée de votre parcours sur " **Ajouter une activité et ressource** " pour y créer une activité du type "**Contenu Interactif**".

![Bouton H5P](../img/creercontenusinteractifspartiecommune/H5Pn.png#printscreen#centrer)

Ou créez l'activité à partir de la banque de contenus.

![Bouton H5P](../img/creercontenusinteractifspartiecommune/H5Pb.png#printscreen#centrer)

3. Dans le formulaire qui s’affiche, après avoir saisi éventuellement une rapide description de l'activité qui va être créée, repérez le type de contenu interactif souhaité dans la section "**Éditeur**" et le menu "**H5P hub - Sélectionnez le type d'activité**" ; puis cliquez sur le bandeau correspondant pour créer la ressource voulue.

Si vous souhaitez davantage d'informations sur la procédure de création d'un contenu interactif, un tutoriel détaillé est disponible ici : [Tutoriel Créer un Contenu Interactif](https://dne-elearning.gitlab.io/moodle-elea/documentation/docs/Tutoriels/Integrer%20des%20contenus%20interactifs/Creer_Contenus_Interactifs/).

## Créer l'arrière-plan de l'activité

![Interface de l'activité Find Mutliple Hotspots Image réactive à points multiples](../img/findmultiplehotspotsimageacliquer/findmultiplehotspotsimageacliquer03interface.png#printscreen#centrer)

1. Donnez un titre à l'activité. C'est sous ce titre que cette activité s'affichera dans le parcours Éléa.
2. Indiquez ici un sous-titre. Ce sous-titre peut permettre de souligner une nuance par rapport au titre précédent, c'est-à-dire une caractéristique qui permettra de discriminer les bonnes des mauvaises réponses dans l'image proposée. 

3. **Image d'arrière-plan** : cet onglet permet de déposer l'image d'arrière-plan de l'activité. 

4. Cliquez sur le bouton "**+ Ajouter**". Puis parcourez les dossiers de votre ordinateur pour y sélectionner le fichier image souhaité. C'est sur cette image que reposera toute l'activité en fournissant en arrière-plan à l'élève les différents éléments à distinguer entre eux et sur lesquels il s'agira de cliquer.

L'image ajoutée à l'activité peut être supprimée (afin par exemple de lui en substituer une autre) avec l'icone croix **x** en haut à droite.

![Icône de suppression d'image](../img/findmultiplehotspotsimageacliquer/findmultiplehotspotsimageacliquer04interface.png#printscreen#centrer)

Sous l'image ajoutée à l'activité un menu **Éditer l'image** apparaît : il permet de rogner l'image ou de la pivoter quart de tour par quart de tour.

![Bouton éditer l'image](../img/findmultiplehotspotsimageacliquer/findmultiplehotspotsimageacliquer05boutonediterimage.png#printscreen#centrer)


![Menu d'édition d'image](../img/findmultiplehotspotsimageacliquer/findmultiplehotspotsimageacliquer06menueditionimages.png#printscreen#centrer)

Enregistrez enfin les modifications appliquées à l'image.

![Bouton d'enregistrement d'une image](../img/findmultiplehotspotsimageacliquer/findmultiplehotspotsimageacliquer07enregistrerimage.png#printscreen#centrer)

**Remarque** : un bouton copyright permet de renseigner les crédits et droits d'usage de l'image. Pour un rappel des principales licences en vigueur [cliquez ici](http://creativecommons.fr/licences/).

![Le bouton pour renseigner les crédits et droits d'usage des images qui apparaissent dans l'album.](../img/findmultiplehotspotsimageacliquer/findmultiplehotspotsimageacliquer08boutoncopyright.png#printscreen#centrer)

5. Cliquez enfin sur  "**Hotspots**" (zones réactives) pour passer à l'onglet suivant et poursuivre la création de l'activité. 

## Créer les "zones réactives"

![Interface de création des Hotspots ou "zones réactives"](../img/findmultiplehotspotsimageacliquer/findmultiplehotspotsimageacliquer09interfacehotspotszonesreactives.png#printscreen#centrer)



1. Rédigez dans ce champ la consigne.

**Exemple** : Cliquez sur les drapeaux des pays qui ne sont pas membres de l'Union Européenne.

2. Indiquez ici le type d'éléments qui est à rechercher dans l'image : cette mention sera employée en complément dans les rétroactions qui suivront chaque clic de l'élève.

**Exemple** : la mention "**des pays hors de l'U.E. cachés ici.**" de l'exemple apparaitra dans des rétroactions automatisées libellées ainsi : "Vous avez trouvé 1 sur 3 **des pays hors de l'U.E. cachés ici**".

3. Renseignez dans ce champ le **nombre minimum** de clics corrects (c'est-à-dire de clics sur des éléments correspondants à la consigne) qui sont attendus pour que l'activité soit considérée comme ayant été réussie par l'élève.

4. **Hotspots** : cliquez sur la forme souhaitée disque ou carré pour la première "zone réactive" à dessiner.

![Fenêtre surgissante des Hotspots ou zones réactives](../img/findmultiplehotspotsimageacliquer/findmultiplehotspotsimageacliquer10fenetresurgissantedeshotspotszonesreactives.png#printscreen#centrer)

Dans la fenêtre surgissante qui apparaît :

1. Si la "zone réactive" sur le point d'être tracée est bien une zone sur laquelle l'élève devra cliquer dans l'activité cochez "**Correct**", sinon laissez décoché.

2. Il est possible de rédiger ensuite une rétroaction spécifique qui surgira après un clic sur cette "zone réactive".

3. Cliquez enfin pour créer la "zone réactive" (ou annulez en cliquant sur la mention en rouge).

## Positionner et dimensionner les "zones réactives"

Les éléments "zones réactives" peuvent être déplacées au-dessus de l'image d'arrière-plan par cliquer-glisser, et peuvent être redimensionnées en tirant sur les "poignées" carrées blanches du pourtour de la zone sélectionnée.

![Positionner et redimensionner les zones réactives](../img/findmultiplehotspotsimageacliquer/findmultiplehotspotsimageacliquer11zonesreactives.png#printscreen#centrer)

**Remarque**: en maintenant la **touche majuscule** du clavier enfoncée et en jouant sur la  poignée inférieure droite de la "zone réactive" choisie, celle-ci sera redimensionnée de manière proportionnelle en longueur et largeur à ses dimensions d'origine. Il est aussi possible d'utiliser les quatre flèches du clavier pour déplacer précisément la "zone réactive" sélectionnée.

Pour rappel ces "zones réactives " resteront totalement invisibles pour l'élève.

Il s'agit donc de placer chaque "zone réactive" transparente en cohérence visuellement avec un des éléments distinctifs de l'image d'arrière-plan choisie (élément correspondant ou non à la consigne selon que la zone a été précédemment déclarée correcte ou non).

Pour un positionnement ou un redimensionnement plus précis on utilisera les outils du menu flottant de la "zone réactive" sélectionnée.

![Menu flottant des "zones réactives"](../img/findmultiplehotspotsimageacliquer/findmultiplehotspotsimageacliquer12menuflottantdeszonesreactives.png#printscreen#centrer)

Il est enfin possible d'améliorer la guidance de l'activité avec deux dernières rétroactions.

![Améliorer la guidance de l'activité](../img/findmultiplehotspotsimageacliquer/findmultiplehotspotsimageacliquer13guidance.png#printscreen#centrer)

1. Ce message s'affichera si l'élève clique sur l'image en dehors de toute "zone réactive".
2. Ce message s'affichera si l'élève clique à nouveau sur une "zone réactive" où il avait précédemment déjà cliqué.

L'activité"**Image à cliquer**" est prête : vous pouvez cliquer en bas de page sur "**ENREGISTRER ET AFFICHER**" pour la tester.

![Bouton enregistrer et afficher](../img/findmultiplehotspotsimageacliquer/findmultiplehotspotsimageacliquer14boutonenregistreretafficher.png#printscreen#centrer)
