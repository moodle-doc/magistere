# Mark the Words H5P (Mots recherchés)



Ce contenu interactif (H5P) permet de créer un texte dans lequel l'élève devra repérer certains mots respectant une consigne donnée. 

![Exemple d'énoncé d'une activité "Mark the Words" (Mots recherchés)](../img/markthewordsmotscliques/markthewordsmotscliques01exempleenonce.png#printscreen#centrer)

**Exemple** : dans cette phrase cliquez sur un adverbe :  "Longtemps, je me suis couché de bonne heure".

Une réponse incorrecte.

![Exemple d'une réponse incorrecte à une activité "Mark the Words" (Mots recherchés)](../img/markthewordsmotscliques/markthewordsmotscliques02exemplefaux.png#printscreen#centrer)

Une réponse correcte.

![Exemple d'une réponse correcte à une activité "Mark the Words" (Mots à cliquer)](../img/markthewordsmotscliques/markthewordsmotscliques03exemplejuste.png#printscreen#centrer)

Nous commencerons par ajouter un contenu interactif du type (en anglais) "**Mark the Words**".

![Icone de l'activité Mark the Words Mots cliqués](../img/markthewordsmotscliques/markthewordsmotscliquesicone.png#printscreen#centrer)

## Ajouter le contenu interactif

1. Activez le mode édition dans votre parcours en cliquant sur ce bouton en haut à droite.

2. Cliquez dans la section souhaitée de votre parcours sur " **Ajouter une activité et ressource** " pour y créer une activité du type "**Contenu Interactif**".

![Bouton H5P](../img/creercontenusinteractifspartiecommune/H5Pn.png#printscreen#centrer)

Ou créez l'activité à partir de la banque de contenus.

![Bouton H5P](../img/creercontenusinteractifspartiecommune/H5Pb.png#printscreen#centrer)

3. Dans le formulaire qui s’affiche, après avoir saisi éventuellement une rapide description de l'activité qui va être créée, repérez le type de contenu interactif souhaité dans la section "**Éditeur**" et le menu "**H5P hub - Sélectionnez le type d'activité**" ; puis cliquez sur le bandeau correspondant pour créer la ressource voulue.

Si vous souhaitez davantage d'informations sur la procédure de création d'un contenu interactif, un tutoriel détaillé est disponible ici : [Tutoriel Créer un Contenu Interactif](https://dne-elearning.gitlab.io/moodle-elea/documentation/docs/Tutoriels/Integrer%20des%20contenus%20interactifs/Creer_Contenus_Interactifs/).

## Créer le texte contenant des "Mots recherchés"

![Interface de création d'une activité Mark the Words (Mots recherchés)](../img/markthewordsmotscliques/markthewordsmotscliques04interface.png#printscreen#centrer)

1. Donnez un titre à l'activité. C'est sous ce titre que cette activité s'affichera dans le parcours Éléa.

2. Rédigez la consigne pour les élèves. Cette consigne doit ici indiquer le type de mots qui sera à repérer dans le texte de l'activité (et sur lesquels il faudra donc cliquer) .

**Exemple** : "Cliquez sur le verbe du 1er groupe qui est à infinitif dans cette phrase".

3. Dans ce champ de texte entrez le texte de l'activité. Chaque mot de ce texte qui correspond à la consigne, et sur lequel il faudra cliquer (car c'est une bonne réponse), doit être **encadré par deux astérisques** : ``` *...*```. 

**Exemple** : "Le menuisier est allé dans son atelier ``` *découper*``` un plancher en bois de merisier." 

Ces astérisques **ne seront pas affichés** à l'écran, mais ils permettent à la plateforme Éléa d'identifier les mots à prendre en compte comme étant les bonnes réponses.

Dans cet exemple, "découper", qui est le verbe du 1er groupe à infinitif recherché, a été marqué comme étant la bonne réponse grâce à la paire d'astérisques ```*...*``` qui l'encadre.

L'activité est créée : vous pouvez cliquer en bas de page sur "**Enregistrer et afficher**" pour la tester.

![Bouton enregistrer et afficher](../img/markthewordsmotscliques/markthewordsmotscliques08boutonenregistrerafficher.png#printscreen#centrer)

Un mot peut être sélectionné en cliquant dessus, puis désélectionné en cliquant à nouveau sur lui.

![Exemple d'énoncé d'une activité Mark the Words (Mots recherchés)](../img/markthewordsmotscliques/markthewordsmotscliques05exempleenonce.png#printscreen#centrer)

Une réponse incorrecte.

![Exemple d'une réponse incorrecte à une activité Mark the Words (Mots recherchés)](../img/markthewordsmotscliques/markthewordsmotscliques06exemplereponsefausse.png#printscreen#centrer)

Une réponse correcte.

![Exemple d'une réponse correcte à une activité Mark the Words (Mots recherchés)](../img/markthewordsmotscliques/markthewordsmotscliques07reponsejuste.png#printscreen#centrer)

**Remarque**  : L'activité peut exiger que plusieurs mots soient repérés et "cliqués" par l'élève :

   - Consigne : "Dans cette phrase cliquez sur les articles".


   - Texte : "```*Le*``` menuisier est allé dans son atelier découper ```*un*``` plancher en bois de merisier".

![Exemple d'une activité Mark the Words (Mots recherchés) à multiples réponses](../img/markthewordsmotscliques/markthewordsmotscliques09reponsesmutliples.png#printscreen#centrer)

**Attention** : ne pas prendre comme bonne réponse un groupe de mots (**exemple :** ```*Le menuisier*```), ou une partie seulement d'un mot (**exemple :** ```*m*```enuisier).

   

## Rédiger des rétroactions et définir les options

![Interface des options d'une activité Mark the Words (Mots recherchés)](../img/markthewordsmotscliques/markthewordsmotscliques10interfaceoptions.png#printscreen#centrer)
1. En cliquant sur "**Feedback général**", il est possible de rédiger des rétroactions qui s'afficheront à la fin de l'activité en fonction du score de l'élève. Par défaut une seule rétroaction est disponible qui s'affichera pour tous les scores pouvant être obtenus de 0% (aucune bonne réponse) à 100% (réussite complète). Cliquez sur "**Ajouter Intervalle**" autant de fois que souhaité pour indiquer de nouvelles valeurs de score qui viendront subdiviser cet intervalle initial couvrant de 0% à 100%. Pour chacune des nouvelles valeurs saisies entre 0 et 100, il sera alors possible de rédiger une rétroaction adaptée. Selon que le score obtenu sera en-deçà ou au-delà de cette valeur donnée (en %), c'est l'une ou autre de ces rétroactions qui s'affichera pour commenter le travail de l'élève au terme de l'activité.

![Interface des rétroactions d'une activité Mark the Words (Mots recherchés)](../img/markthewordsmotscliques/markthewordsmotscliques11retroactions.png#printscreen#centrer)

2. **Options Générales** : il s'agit de définir si les boutons "**Voir la solution**" et "**Recommencer**" seront ou non rendus disponibles pour l'élève, et si le score en points qui a été obtenu sera ou non affiché.

![Trois options d'une activité Mark the Words (Mots recherchés)](../img/markthewordsmotscliques/markthewordsmotscliques12options.png#printscreen#centrer)

3. Le menu "**Options et Textes**" permet par exemple de modifier les libellés des boutons de l'activité,  ou encore, les mentions affichées en cas de bonne ou mauvaise réponse.
