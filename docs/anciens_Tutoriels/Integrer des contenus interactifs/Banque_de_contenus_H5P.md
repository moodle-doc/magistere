# Gérer la banque de contenus H5P

La banque de contenus H5P est accessible dans tous les parcours via le menu principal du parcours en haut à droite, dans les **"paramètres du parcours"** en cliquant sur le menu **"Banque de contenus"**.

![menu](../img/banque_de_contenus_H5P/menu.png#printscreen#centrer)

Cette page vous permet de **gérer toutes les activités H5P de votre parcours** en amont de la médiatisation de votre parcours ou par la suite.

Pour y ajouter des activités H5P, deux solutions sont possibles :

- Pour créer une nouvelle activité H5P, il suffit de cliquer sur le menu **"Ajouter"**. Vous retrouverez l'interface classique vous permettant d'éditer votre H5P (en revanche, il n'est pas possible de changer de type d'activité une fois celle-ci créée). Une fois l'activité H5P complétée, vous cliquez sur Enregistrer au bas de la page.

- Si vous disposez d'un fichier H5P existant, il est possible de l'importer en cliquant sur **"Déposer"**.

![ecran1](../img/banque_de_contenus_H5P/fenetre1.png#printscreen#centrer)

Deux modes d'affichage sont disponibles : Vue avec des **icônes** ou vue avec le **détail des fichiers**.

![ecran2](../img/banque_de_contenus_H5P/fenetre2.png#printscreen#centrer)

Sur cette page, vous pourrez afficher votre H5P en cliquant dessus. Des menus supplémentaires apparaissent sur la droite pour gérer les H5P :

- Le menu **"Modifier"** vous permet d'éditer votre H5P.

- un petit **"engrenage"** vous permet d'afficher un menu supplémentaire avec 5 options : **Rendre non listé** pour cacher l'activité H5P, **renommer** le fichier, **remplacer** le fichier par un autre H5P, **télécharger** le fichier sur votre ordinateur, et enfin **supprimer** le fichier.

![ecran3](../img/banque_de_contenus_H5P/fenetre3.png#printscreen#centrer)


## Comment intégrer les H5P de la banque de contenus dans son parcours ?

Vous pourrez ensuite intégrer vos H5P dans votre parcours en insérant une activité H5P bleue.

![H5P_bleue](../img/banque_de_contenus_H5P/H5Pbleu.png#printscreen#centrer)

Vous devez compléter le titre de l'activité et pouvez entrer une petite description, puis vous **cliquez** dans la zone permettant de déposer un fichier.

(NB: Un lien vers la banque de contenus est disponible en dessous et vous permet d'afficher la banque de contenus dans un nouvel onglet.)

![ecran4](../img/banque_de_contenus_H5P/fenetre4.png#printscreen#centrer)

Une fenêtre s'affiche vous permettant de sélectionner la banque de contenus et les fichiers qu'elle contient.

![ecran5](../img/banque_de_contenus_H5P/fenetre5.png#printscreen#centrer)

Dans cette fenêtre, les 2 modes d'affichage présents dans la banque de contenus sont disponibles (icônes et détail des fichiers). Un autre bouton **"Afficher le dossier sous la forme d'arbre de fichiers"** permet d'afficher **l'ensemble des parcours** dans lequel vous êtes inscrit, et vous permet de sélectionner **les banques de contenus** d'autres parcours pour réutiliser un H5P disponible dans un autre parcours.

![ecran6](../img/banque_de_contenus_H5P/fenetre6.png#printscreen#centrer)

Finalisez les paramétrages de l'activité si nécessaire (options, achèvement, restriction, ...) comme sur toute autre activité de la plateforme éléa. Une option supplémentaire H5P vous permet d'autoriser le téléchargement, l'intégration ou d'afficher un copyright.

![ecran7](../img/banque_de_contenus_H5P/fenetre7.png#printscreen#centrer)

Pour tester votre contenu interactif, cliquez en bas de page sur "Enregistrer et afficher".
