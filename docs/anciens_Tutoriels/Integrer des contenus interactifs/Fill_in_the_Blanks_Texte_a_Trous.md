# Fill in the Blanks H5P (Texte à "trous")



Ce contenu interactif (H5P) permet de créer des textes à "trous" : ce sera à l'élève de compléter correctement ces "blancs" du texte en fonction du contexte.

![Un exemple d'énoncé "Fill in the Blanks"](../img/fillintheblankstexteatrous/fillintheblankstexteatrous01exemple.png#printscreen#centrer)

Une réponse incorrecte.

![Un exemple de réponse fausse](../img/fillintheblankstexteatrous/fillintheblankstexteatrous02exemplereponsefausse.png#printscreen#centrer)

La réponse correcte.

![Un exemple de réponse juste](../img/fillintheblankstexteatrous/fillintheblankstexteatrous03exemplereponsejuste.png#printscreen#centrer)

Nous commencerons par créer un contenu interactif de type (en anglais) "**Fill in the Blanks**".

![Icone de l'activité Fill in the Blanks Texte à Trous](../img/fillintheblankstexteatrous/fillintheblankstexteatrousicone.png#printscreen#centrer)

## Ajouter le contenu interactif

1. Activez le mode édition dans votre parcours en cliquant sur ce bouton en haut à droite.

2. Cliquez dans la section souhaitée de votre parcours sur " **Ajouter une activité et ressource** " pour y créer une activité du type "**Contenu Interactif**".

![Bouton H5P](../img/creercontenusinteractifspartiecommune/H5Pn.png#printscreen#centrer)

Ou créez l'activité à partir de la banque de contenus.

![Bouton H5P](../img/creercontenusinteractifspartiecommune/H5Pb.png#printscreen#centrer)

3. Dans le formulaire qui s’affiche, après avoir saisi éventuellement une rapide description de l'activité qui va être créée, repérez le type de contenu interactif souhaité dans la section "**Éditeur**" et le menu "**H5P hub - Sélectionnez le type d'activité**" ; puis cliquez sur le bandeau correspondant pour créer la ressource voulue.

Si vous souhaitez davantage d'informations sur la procédure de création d'un contenu interactif, un tutoriel détaillé est disponible ici : [Tutoriel Créer un Contenu Interactif](https://dne-elearning.gitlab.io/moodle-elea/documentation/docs/Tutoriels/Integrer%20des%20contenus%20interactifs/Creer_Contenus_Interactifs/).

## Créer le texte à "trous"

![Interface de l'activité "Fill in the Blanks" (Texte à trous)](../img/fillintheblankstexteatrous/fillintheblankstexteatrous04interfacefillintheblanks.png#printscreen#centrer)

1. Donnez un titre à l'activité. C'est sous ce titre que cette activité s'affichera dans le parcours Éléa.

2. Rédigez la consigne pour les élèves. 

**Exemple** : "Complétez cette phrase avec le nombre correct."

3. Dans ce champ de texte entrez le texte de l'activité. Pour créer les "blancs" qui devront être complétés par l'élève, **encadrez par deux astérisques** ```*...*``` les mots à effacer. 

**Exemple** : "Paul a sept billes de plus que Damien. Paul a trente-trois billes. Donc Damien a ```*vingt-six*``` billes."

Ces astérisques ```*...*``` **ne seront pas affichés** à l'écran : ils permettent à la plateforme Éléa d'identifier les mots à effacer, tout en lui fournissant les réponses correctes - ce sont ces réponses entre astérisques ```*...*``` qui seront comparées avec celles de l'élève pour établir son score.

Dans l’exemple, "vingt-six" a été marqué comme étant la bonne réponse grâce à la paire d'astérisques ```*...*``` qui l'encadrent : il n'apparaîtra pas à écran, et il sera attendu de l'élève qu'il saisisse "vingt-six" dans le "blanc" ainsi créé.

L'activité est prête : vous pouvez cliquer en bas de page sur "**Enregistrer et afficher**" pour la tester.

![fill intheblankstexteatrous05interfacefillintheblanks](../img/fillintheblankstexteatrous/fillintheblankstexteatrous05interfacefillintheblanks.png#printscreen#centrer)



**Remarque** : Le texte proposé peut inclure plusieurs "blancs" à compléter. Un "blanc" peut inclure plusieurs mots.



**Exemple** : Ce texte "Paul a sept billes de plus que Damien. Damien a onze billes de moins que Marie. Paul a trente-trois billes. Donc Damien a ```*vingt-six*``` billes et Marie a ```*trente-sept*``` billes" produira cet affichage.

![Un exemple de texte contenant plusieurs "blancs"](../img/fillintheblankstexteatrous/fillintheblankstexteatrous06reponsesmutliples.png#printscreen#centrer)



## Créer des réponses alternatives

Parfois plusieurs réponses sont acceptables pour compléter un "blanc" du texte proposé.

Toutes les réponses alternatives envisagées doivent être listées entre les astérisques ```*...*``` mais en étant séparées entre elles par des slashs ```/```.

**Exemple** : "Paul a sept billes de plus que Damien. Paul a trente-trois billes. Donc Damien a ```*vingt-six/26*```billes."

Ici la réponse pourra être fournie aussi bien en toutes lettres qu'en chiffres.

![Un texte à trous qui accepte plusieurs réponses correctes](../img/fillintheblankstexteatrous/fillintheblankstexteatrous07plusieursreponsespossibles.png#printscreen#centrer)

![Un texte à trous qui accepte plusieurs réponses correctes](../img/fillintheblankstexteatrous/fillintheblankstexteatrous08plusieursreponsespossibles.png#printscreen#centrer)

## Créer un indice

L'enseignant peut souhaiter glisser un "coup de pouce" pour guider l'élève.

Le texte de cet indice doit être placé entre les astérisques ```*...*```, après la ou les réponses prévue(s), et cet indice doit être précédé par **deux points de ponctuation : **.

**Exemple** : "Paul a sept billes de plus que Damien. Paul a trente-trois billes. Donc Damien a ```*vingt-six/26:Qui a le plus de billes : Paul ou Damien ?*``` billes."

![Exemple acceptant plusieurs réponses correctes possibles](../img/fillintheblankstexteatrous/fillintheblankstexteatrous09plusieursreponsespossibles.png#printscreen#centrer)

Pour chaque "blanc" pour lequel un indice a été prévu, l'élève pourra afficher cet indice dans une bulle d'aide en cliquant sur l'icône **i** associée.

## Créer des paragraphes

1. Cliquez sur "**Ajouter Bloc de Texte**" pour créer un nouveau paragraphe.

2. Ces paragraphes peuvent être ensuite déplacés entre eux (montés en cliquant à droite sur **▲**, ou descendus en cliquant sur **▼**), ou même supprimés en cliquant sur la croix **x**.

![Les icones pour créer des paragraphes](../img/fillintheblankstexteatrous/fillintheblankstexteatrous10paragraphes.png#printscreen#centrer)

## Ajouter un média

Il est possible d'afficher en entête de l'activité une image ou une vidéo pour illustrer le texte à "trous" qui est à compléter.

1. Cliquez sur la bandeau grisé "**Fichier média**".

![Bandeau Ficher Média](../img/fillintheblankstexteatrous/fillintheblankstexteatrous11fichiermedia.png#printscreen#centrer)

2. Dans le menu déroulant "**Type de Média**" sélectionner "**Image**" ou "**Vidéo**" (ou sélectionnez le tiret ▬ pour supprimer éventuellement un média que vous auriez précédemment ajouté).

### Pour une image

![Menu d'ajout d'un fichier image](../img/fillintheblankstexteatrous/fillintheblankstexteatrous12fichiermediaimage.png#printscreen#centrer)

1. Cliquez sur le bouton ajouter. Puis parcourez les dossiers de votre ordinateur pour y sélectionner le fichier image souhaité. Un menu **Éditer l'image** apparaît : il permet de rogner l'image ou de la pivoter quart de tour par quart de tour.

![Bouton éditer l'image](../img/fillintheblankstexteatrous/fillintheblankstexteatrous13boutonediterimage.png#printscreen#centrer)


![Menu d'édition d'image](../img/fillintheblankstexteatrous/fillintheblankstexteatrous14editionimage.png#printscreen#centrer)

Enregistrez enfin les modifications appliquées à l'image.

![Bouton d'enregistrement d'une image](../img/fillintheblankstexteatrous/fillintheblankstexteatrous15enregistrementimage.png#printscreen#centrer)


2. Rédigez un **Texte alternatif** : ce texte se substituera à l'image si celle-ci rencontre un quelconque problème d'affichage quand l'activité est consultée.

3. (optionnel) **Texte de survol** : rédigez ici un texte qui s'affichera sous forme de bulle volante quand le pointeur de la souris survolera l'image.

4. **Désactiver l'agrandissement de l'image** : si cette option est cochée l'image sera affichée avec ses dimensions d'origine. En revanche en décochant cette option, une icône **+** et **-** dans le coin supérieur droit de l'image permettra à l'élève d'en faire varier les dimensions (zoom et dézoom).

![Exemple de zoom](../img/fillintheblankstexteatrous/fillintheblankstexteatrous16exemplezoom.png#printscreen#centrer)



### Pour une vidéo

![Menu d'ajout d'un fichier vidéo](../img/fillintheblankstexteatrous/fillintheblankstexteatrous17fichiermediavideo.png#printscreen#centrer)

1. Indiquez un titre pour la vidéo qui précèdera le texte "à trous".
2. Cliquez sur le signe **+** pour explorer vos fichiers et y sélectionner le fichier vidéo à déposer. Alternativement, vous pouvez aussi entrer directement l'adresse (url) d'une vidéo hébergée en ligne. Cliquez ensuite sur "**Insérer**".
3. Ce menu permet selon les options cochées :
      - d'insérer une image qui se substituera à la vidéo hors lecture.
      - de définir si le lecteur vidéo occupera la plus grande largeur possible dans la page où il s'affichera, ou s'il s'adaptera aux dimensions de la vidéo diffusée.
      -  de choisir si les contrôles de lecture, pause et arrêt apparaîtront ou non.
4. La lecture peut démarrer automatiquement au chargement du lecteur et/ou boucler une fois arrivée en fin de vidéo selon les options cochées dans ce menu.
5. Vous pouvez enfin déposer ici un fichier de sous-titres pour améliorer l'accessibilité de votre contenu vidéo. Ce fichier de sous-titres doit être au format WebVTT (pour en apprendre davantage sur ce format de fichier [cliquez ici](https://fr.wikipedia.org/wiki/WebVTT).

## Rédiger des rétroactions et définir les options

![Menus des options d'un media vidéo](../img/fillintheblankstexteatrous/fillintheblankstexteatrous18optionsmediavideo.png#printscreen#centrer)

1. En cliquant sur "**Feedback général**" il est possible de rédiger des rétroactions qui s'afficheront à la fin de l'activité en fonction du score de l'élève. Par défaut une seule rétroaction est disponible qui s'affichera pour tous les scores pouvant être obtenus de 0% (aucune bonne réponse) à 100% (réussite complète). Cliquez sur "**Ajouter Intervalle**" autant de fois que souhaité pour indiquer de nouvelles valeurs de score qui viendront subdiviser cet intervalle initial couvrant de 0% à 100%. Pour chacune des nouvelles valeurs saisies entre 0 et 100, il sera alors possible de rédiger une rétroaction adaptée. Selon que le score obtenu sera en-deçà ou au-delà de cette valeur donnée(en %), c'est l'une ou autre de ces rétroactions qui s'affichera pour commenter le travail de l'élève au terme de l'activité.

2. **Options Générales** : il s'agit de définir si les boutons "**Voir les solutions**" et "**Recommencer**" seront ou non rendus disponibles pour l'élève, et si le score en points qui a été obtenu sera ou non affiché.

3. Le menu "**Options et Textes**" permet par exemple de modifier les libellés des boutons de l’activité, ou encore les mentions affichées en cas de bonne ou mauvaise réponse.

(Crédits image : pixabay.com)
