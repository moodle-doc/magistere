// @ts-check
// `@type` JSDoc annotations allow editor autocompletion and type checking
// (when paired with `@ts-check`).
// There are various equivalent ways to declare your Docusaurus config.
// See: https://docusaurus.io/docs/api/docusaurus-config

import {themes as prismThemes} from 'prism-react-renderer';

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Documentation pour l\'utilisation de Magistère',
  tagline: 'Au service du développement de mes compétences',
  favicon: '/img/magistere.ico',

  // Set the production url of your site here
  url: 'https://moodle-doc.forge.apps.education.fr/',
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: '/magistere/',


  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',

  // Even if you don't use internationalization, you can use this field to set
  // useful metadata like html lang. For example, if your site is Chinese, you
  // may want to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'fr',
    locales: ['fr'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: './sidebars.js',
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
         },
        blog: {
          showReadingTime: true,
          blogSidebarTitle:'Derniers articles'
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
        },
        theme: {
          customCss: './src/css/custom.css',
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      // Replace with your project's social card
      image: '/img/magistere_long.png',
      navbar: {
        title: '',
        logo: {
          alt: 'logo de Magistere',
          src: '/img/magistere_long.png',
        },
        items: [
          {
            type: 'docSidebar',
            sidebarId: 'SidebarTutoriels',
            position: 'left',
            label: 'Tutoriels',
          },
          {to: 'showcase', label: 'Usages', position: 'left'},
          //{type: 'doc',docId: 'FAQ',label: '❓FAQ',position: 'left',},
          {label: 'Communauté',href: 'https://magistere.education.fr/dgesco/course/view.php?id=1422',position: 'left',},
          {to: 'blog', label: 'Nouveautés', position: 'left'},
        ],

      },
      algolia: {
        // L'ID de l'application fourni par Algolia
        appId: 'D8X21ZDUOW',
  
        // Clé d'API publique : il est possible de la committer en toute sécurité
        apiKey: 'bcddf88fcc7d6c7fb89990dc4d1e3fd2',
  
        indexName: 'dne-elearning-gitlab',
  
        // Facultatif : voir la section doc ci-dessous
        contextualSearch: true,

        // Facultatif : paramètres de recherche de Algolia
        searchParameters: {},
  
        // Facultatif : chemin pour la page de recherche qui est activée par défaut (`false` pour le désactiver)
        searchPagePath: 'search',
  
        //... autres paramètres de Algolia
      },
      footer: {
        style: 'dark',
        links: [{label:'Accéder à Magistère', href:'https://magistere.apps.education.fr', position:'center' }],
        copyright: `Équipe Magistère nationale, Ministère de l'Éducation nationale`,
      },
      prism: {
        theme: prismThemes.github,
        darkTheme: prismThemes.dracula,
      },
    }),

  plugins: [
    [
      '@docusaurus/plugin-ideal-image',
      {
        quality: 70,
        max: 1030, // max resized image's size.
        min: 640, // min resized image's size. if original is lower, use that size.
        steps: 2, // the max number of images generated between min and max (inclusive)
        disableInDev: false,
      },
    ],
  ],
};

export default config;
